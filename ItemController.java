package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.Item;
import com.hth.lh.persistence.model.ItemPicture;
import com.hth.lh.persistence.model.Member;
import com.hth.lh.service.CategoryService;
import com.hth.lh.service.ItemService;
import com.hth.lh.web.model.*;
import com.hth.lh.web.util.IPAddressResolver;
import com.hth.lh.web.util.ImageTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.WebUtils;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/4/27
 * Time: 13:22
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private CategoryService categoryService;





    //获得所有未指定item的图片
    @RequestMapping(method = RequestMethod.GET,value = {"/items/pictures"})
    @ResponseBody
    public JsonResponse getUndeclaredItemPicutes(HttpSession session) {
        JsonResponse jsonResponse = new JsonResponse();
        Member member = (Member)session.getAttribute(SessionKey.SESSION_MEMBER);
        List<ItemPicture> undeclaredItemPicutes = null;
        if (member==null) {
            undeclaredItemPicutes = itemService.getUndelcaredItemPictures(null);
        }
        else {
            undeclaredItemPicutes = itemService.getUndelcaredItemPictures(member.getMemberid());
        }
        jsonResponse.setContent(undeclaredItemPicutes);
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        return jsonResponse;
    }

    //获得指定itemid的图片

    @RequestMapping(method = RequestMethod.GET,value = {"/items/itemid-{itemid}/pictures"})
    @ResponseBody
    public JsonResponse getItemPicturesByItemid(@PathVariable(value = "itemid") Long itemid) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(itemService.getItemPicturesByItemid(itemid));
        return jsonResponse;
    }


    //avatar,carousels
    //新增提交图片
    @RequestMapping(method = RequestMethod.POST,value = {"/items/picture-{picturetype}-new"})
    @ResponseBody
    public JsonResponse uploadAvatarPicture(@PathVariable String picturetype, @RequestParam(value = "avatarpicture") MultipartFile avatarPicture,
            @RequestParam(required = false) Long itemid,HttpSession session) {
        JsonResponse jsonResponse = new JsonResponse();

        if (ImageTool.imageMIME.contains(avatarPicture.getContentType())) {
            try {
                BufferedImage bufferedImage = ImageIO.read(avatarPicture.getInputStream());
                Member member = (Member)session.getAttribute(SessionKey.SESSION_MEMBER);
                Long memberid = null;
                if (member!=null) {
                    memberid = member.getMemberid();
                }
                ItemPicture itemPicture = itemService.createItemImage(avatarPicture.getOriginalFilename(),bufferedImage,picturetype,memberid,itemid);
                if (itemPicture!=null) {
                    jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
                    jsonResponse.setContent(itemPicture);
                }
                else {
                    jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
                }

            } catch (IOException e) {
                jsonResponse.setStatus(JsonResponse.RESPONSE_EXCEPTION);
                jsonResponse.setErrorMessage("Can not read from uploading stream.");
            }


        }
        else {
            jsonResponse.setStatus(JsonResponse.RESPONSE_FIELD_ERROR);
            jsonResponse.setErrorMessage("Content type not acceptable");
        }
        return jsonResponse;
    }

    //删除指定图片
    @RequestMapping(value = {"/items/picture-remove-{pictureid}"})
    @ResponseBody
    public JsonResponse removePicture(@PathVariable String pictureid) {
        JsonResponse jsonResponse = new JsonResponse();
        if (itemService.deleteItemImage(pictureid)) {
            jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        }
        else {
            jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
        }
        return jsonResponse;
    }

    //新增商品页面
    @RequestMapping(method = RequestMethod.GET,value = {"/items/item-new"})
    public String addItemPage(Model model) {
        return "item-new";
    }


    //新增商品提交
    @RequestMapping(method = RequestMethod.POST,value = {"/items/item-new"})
    public String addItemSubmit(ItemForm itemForm, HttpServletRequest request,HttpSession session) {

        Member member = (Member) session.getAttribute(SessionKey.SESSION_MEMBER);
        Long memberid = null;
        if (member!=null) {
            memberid = member.getMemberid();
        }
        itemService.addItem(itemForm, IPAddressResolver.getIpAddress(request), memberid);
        return "redirect:/items";
    }

    //编辑商品页面
    @RequestMapping(method = RequestMethod.GET,value = {"/items/item-edit-{itemid}"})
    public String getItemEditPage(@PathVariable Long itemid,HttpServletRequest request,Model model) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        Long memberid = null;
        if (member!=null) {
            memberid = member.getMemberid();
        }
        //查询指定商品信息,分类、属性选项信息
        Item item = itemService.getItemByItemidWithCategoryAndOption(itemid,memberid);
        if (item==null) {
            return "pagenotfound";
        }
        model.addAttribute("item",item);
        return "item-edit";
    }

    @RequestMapping(method = RequestMethod.POST,value = {"/items/item-edit-{itemid}"})
    public String submitItemEditInfo(@PathVariable Long itemid,ItemForm itemForm, HttpServletRequest request) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        Long memberid = null;
        if (member!=null) {
            memberid = member.getMemberid();
        }
        itemService.editItem(itemid, itemForm, IPAddressResolver.getIpAddress(request), memberid);
        return "redirect:/items";
    }

    //商品列表
    @RequestMapping(method = RequestMethod.GET,value = {"/items"})
    public String getItemsPage(ItemFiltersForm itemFiltersForm,
                               @RequestParam(required = false,defaultValue = "1",value = "pagenumber") Integer pageNumber,HttpSession session,Model model) {
        Member member = (Member) session.getAttribute(SessionKey.SESSION_MEMBER);
        Long memberid = null;
        if (member!=null) {
            memberid = member.getMemberid();
        }

        PageModel<Item> pageModel = itemService.getItemsByFilters(itemFiltersForm,memberid,pageNumber,1);
        model.addAttribute("pageModel", pageModel);

        return "items";
    }

    //改变商品的状态，删除商品
    @RequestMapping(value = {"/items/item-status-{itemid}"})
    public String removeItem(@PathVariable Long itemid,
                             @RequestParam(required = false) Boolean isdeleted,
                             @RequestParam(required = false) String status) {

        itemService.deleteOrChangeItemStatus(itemid,isdeleted,status);
        return "redirect:/items";
    }





}
