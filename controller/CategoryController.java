package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.*;
import com.hth.lh.service.CategoryService;
import com.hth.lh.web.model.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/4/29
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class CategoryController {


    @Autowired
    private CategoryService categoryService;


    //打开分类管理页面
    @RequestMapping(method = RequestMethod.GET, value = "/categories")
    public String getCategoriesPage() {
        return "categories";
    }


    //获得整棵树图的json数据
    @RequestMapping(method = RequestMethod.GET, value = "/categories/category-tree")
    @ResponseBody
    public JsonResponse getCategoryBrandTree() {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(categoryService.getAllCategory());
        return jsonResponse;
    }

    //获得品牌列表的json数据
    @RequestMapping(method = RequestMethod.GET, value = "/categories/brand-list")
    @ResponseBody
    public JsonResponse getCategoryList() {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(categoryService.getAllBrand());
        return jsonResponse;
    }

    //提交分类增删改数据
    //节点的顺序sort>=0
    @RequestMapping(method = RequestMethod.POST, value = "/categories/category-all")
    @ResponseBody
    public JsonResponse addUpdateDeleteCategory(Category category) {
        JsonResponse jsonResponse = new JsonResponse();
        categoryService.addUpdateDeleteCategoryAndSort(category);
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(categoryService.getAllCategory());
        return jsonResponse;
    }


    //提交品牌增删改数据
    @RequestMapping(method = RequestMethod.POST, value = "/categories/brand-all")
    @ResponseBody
    public JsonResponse addUpdateDeleteBrand(Brand brand) {
        JsonResponse jsonResponse = new JsonResponse();
        categoryService.addUpdateDeleteBrandAndSort(brand);
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(categoryService.getAllCategory());
        return jsonResponse;
    }

    //提交品牌分类映射增删改数据
    //品牌映射分类只能对叶子节点添加删除
    //父节点根据叶子节点的情况自动添加删除
    //父节点可以调整顺序
    @RequestMapping(method = RequestMethod.POST, value = "/categories/brand-category")
    @ResponseBody
    public JsonResponse addUpdateDeleteBrandCategory(BrandCategoryMap brandCategoryMap) {
        JsonResponse jsonResponse = new JsonResponse();
        categoryService.addUpdateDeleteBrandCategoryMapAndSort(brandCategoryMap);
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(categoryService.getAllCategory());
        return jsonResponse;
    }


    //提交分类属性增删改数据
    @RequestMapping(method = RequestMethod.POST, value = "/categories/category-property")
    @ResponseBody
    public JsonResponse addUpdateDeleteCategoryProperty(CategoryProperty categoryProperty) {
        JsonResponse jsonResponse = new JsonResponse();
        categoryService.addUpdateDeleteCategoryProperty(categoryProperty);
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(categoryService.getAllCategory());
        return jsonResponse;
    }

    //提交属性选择增删改数据
    @RequestMapping(method = RequestMethod.POST, value = "/categories/property-option")
    @ResponseBody
    public JsonResponse addUpdateDeletePropertyOption(PropertyOption propertyOption) {
        JsonResponse jsonResponse = new JsonResponse();
        categoryService.addUpdateDeletePropertyOption(propertyOption);
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(categoryService.getAllCategory());
        return jsonResponse;
    }

    //重载内存中的分类树形图数据结构缓存
    @RequestMapping(value = "/categories/reload")
    @ResponseBody
    public JsonResponse reload() {
        JsonResponse jsonResponse = new JsonResponse();
        categoryService.reload();
        jsonResponse.setContent(categoryService.getAllCategory());
        return jsonResponse;
    }



}
