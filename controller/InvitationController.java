package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.Authorization;
import com.hth.lh.persistence.model.Category;
import com.hth.lh.persistence.model.Invitation;
import com.hth.lh.persistence.model.Member;
import com.hth.lh.service.AuthorizationService;
import com.hth.lh.service.CartService;
import com.hth.lh.service.CategoryService;
import com.hth.lh.service.InvitationService;
import com.hth.lh.web.model.JsonResponse;
import com.hth.lh.web.model.PageModel;
import com.hth.lh.web.model.SessionKey;
import com.hth.lh.web.util.IPAddressResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/4/24
 * Time: 10:27
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class InvitationController {
    @Autowired
    private InvitationService invitationService;

    @Autowired
    private CartService cartService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AuthorizationService authorizationService;

    //发出的邀请页面查看
    @RequestMapping(method = RequestMethod.GET, value = {"/invitations-mine"})
    public String getInvitationsPage(@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date beginTime,
                                     @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endTime,
                                      @RequestParam(required = false,defaultValue = "1") Integer pageNumber,HttpServletRequest request,Model model) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);

        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));

        PageModel<Invitation> pageModel = invitationService.getInvitationsByFilters(beginTime, endTime,pageNumber, 2, member);
        model.addAttribute("pageModel", pageModel);
        model.addAttribute("latestInvitationCode", invitationService.getLatestEffectiveInvitationCode(member));

        return "invitations-mine";
    }

    //生成新的邀请，老的邀请不作废
    @RequestMapping( value = {"/invitations-mine/invitation-new"})
    public String generateNewInvitationCode(@RequestParam(defaultValue = "100") Integer discount,HttpServletRequest request) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        invitationService.createNewInvitation(discount, member, IPAddressResolver.getIpAddress(request));
        return "redirect:/invitations-mine";
    }

    //作废邀请
    @RequestMapping( value = {"/invitations-mine/invitation-discard-{invitationid}"})
    @ResponseBody
    public JsonResponse discardInvitionByInvitationid(@PathVariable Long invitationid,HttpServletRequest request) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        JsonResponse jsonResponse = new JsonResponse();
        if (invitationService.discardMemberInvitation(invitationid, member,IPAddressResolver.getIpAddress(request))) {
            jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        } else {
            jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
        }
        return jsonResponse;
    }


    //查看使用邀请注册的会员
    @RequestMapping(method = RequestMethod.GET, value = {"/invitations-mine/invitation-register"})
    public String getInvitationRegisterMembers(@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date beginTime,
                                     @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endTime,
                                     @RequestParam(required = false,defaultValue = "1") Integer pageNumber,HttpServletRequest request,Model model) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);

        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));

        PageModel<Authorization> pageModel = invitationService.getInvitationRegisterMemberByFilters(member, beginTime, endTime, pageNumber, 2);
        model.addAttribute("pageModel", pageModel);

        return "invitation-register";
    }

    //查看自己的授权
    @RequestMapping(method = RequestMethod.GET, value = {"/authorization-mine"})
    public String getAuthorizationMinePage(HttpServletRequest request,Model model) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));

        Authorization authorization = authorizationService.getAuthorizationByMember(member);
        model.addAttribute("authorization", authorization);
        return "authorization-mine";

    }




}
