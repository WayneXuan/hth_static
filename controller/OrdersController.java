package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.*;
import com.hth.lh.service.CartService;
import com.hth.lh.service.CategoryService;
import com.hth.lh.service.ConsigneeService;
import com.hth.lh.service.OrdersService;
import com.hth.lh.web.model.*;
import com.hth.lh.web.util.IPAddressResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/5/8
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CartService cartService;


    @Autowired
    private ConsigneeService consigneeService;

    //后台管理供货商订单页面
    @RequestMapping(value = "/orders")
    public String getOrdersWithFilters(@RequestParam(required = false) String keyword,
                                       @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date beginTime,
                                       @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endTime,
                                       @RequestParam(required = false) String status,
                                       @RequestParam(required = false,defaultValue = "1") Integer pageNumber,
                                       Model model,HttpSession session) {
        Member member = (Member) session.getAttribute(SessionKey.SESSION_MEMBER);
        Long memberid = null;
        if (member!=null) {
            memberid = member.getMemberid();
        }
        PageModel<Orders> pageModel = ordersService.getOrdersWithFilters(keyword, beginTime, endTime,memberid,status , pageNumber, 2);
        model.addAttribute("pageModel", pageModel);

        return "orders";
    }

    //后台管理订单作废
    @RequestMapping(value = "/orders/order-discard-{ordersid}")
    @ResponseBody
    public JsonResponse discardOrder(@PathVariable Long ordersid) {
        JsonResponse jsonResponse = new JsonResponse();
        boolean operateFlag = ordersService.changeOrdersStatus(ordersid, Orders.Status.Discarded);
        if (operateFlag) {
            jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        } else {
            jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
        }
        return jsonResponse;
    }


    //供货商查看订单详情
    @RequestMapping(value = "/orders/order-{ordersid}",method = RequestMethod.GET)
    public String getProviderRelatedOrders(@PathVariable Long ordersid, Model model,HttpSession session) {
        Member member = (Member) session.getAttribute(SessionKey.SESSION_MEMBER);
        Orders orders = ordersService.getOrdersDetailByOrdersidWithItemProvided(ordersid, member);
        if (orders != null) {
            model.addAttribute("orders", orders);
            return "orders-provider-detail";
        } else {
            return "pagenotfound";
        }


    }

    //会员查看订单详情
    @RequestMapping(value = {"/orders-mine/order-{ordersSerialid}","/m/orders-mine/order-{ordersSerialid}"},method = RequestMethod.GET)
    public String getMyOrder(@PathVariable String ordersSerialid, Model model,HttpSession session) {
        Member member = (Member) session.getAttribute(SessionKey.SESSION_MEMBER);
        Orders orders = ordersService.getOrdersDetailByOrdersSerialidWithMemberRelated(ordersSerialid, member);
        if (orders != null) {
            model.addAttribute("orders", orders);
            List<Category> categoryList = categoryService.getAllCategory();
            model.addAttribute("categoryList", categoryList);
            model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
            model.addAttribute("addressProvinceMap", consigneeService.getAddressProvinceMap());
            model.addAttribute("addressCityMap", consigneeService.getAddressCityMap());
            model.addAttribute("addressAreaMap", consigneeService.getAddressAreaMap());
            return "orders-mine-detail";
        } else {
            return "pagenotfound";
        }
    }

    //代理更改指定订单号的订单状态
    @RequestMapping(value = {"/orders-mine/order-{ordersSerialid}","/m/orders-mine/order-{ordersSerialid}"}, method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse updateOrdersStatus(@PathVariable String ordersSerialid, @RequestParam Orders.Status status,HttpServletRequest request) {
        JsonResponse jsonResponse = new JsonResponse();
        if (status.equals(Orders.Status.Submitted)) {
            jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
        } else {
            Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
            boolean operateFlag = ordersService.changeOrderStatusByDelegate(ordersSerialid, status, member,IPAddressResolver.getIpAddress(request));
            if (operateFlag) {
                jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
            } else {
                jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
            }
        }
        return jsonResponse;
    }


    //前台会员订单确认页面
    @RequestMapping(value = {"/orders-mine/orders-placement","/m/orders-mine/orders-placement"},method = RequestMethod.GET)
    public String getPlaceOrderForCartItemsPage(HttpServletRequest request,Model model) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);

        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
        String deviceType = DeviceKey.DEVICE_MOBILE;
        if (DeviceUtils.getCurrentDevice(request).isNormal()) {
            deviceType = DeviceKey.DEVICE_PC;
        }
        //获得购物车商品信息
        List<Cart> cartList = cartService.getCartListWithItemDetail(member, deviceType);
        //购物车为空，无法结算
        if (cartList.size() == 0) {
            if (DeviceUtils.getCurrentDevice(request).isNormal()) {
                return "redirect:/";
            } else {
                return "redirect:/m/";
            }
        }
        else {
            model.addAttribute("cartList", cartList);
            //获得收货人地址
            Long memberid = null;
            if (member != null) {
                memberid = member.getMemberid();
            }
            List<Consignee> consigneeList = consigneeService.getConsigneeListByMemberid(memberid);
            if (consigneeList != null) {
                model.addAttribute("consigneeList", consigneeList);
                model.addAttribute("addressProvinceMap", consigneeService.getAddressProvinceMap());
                model.addAttribute("addressCityMap", consigneeService.getAddressCityMap());
                model.addAttribute("addressAreaMap", consigneeService.getAddressAreaMap());
            }

            return "orders-placement";
        }
    }

    //前台会员订单确认提交
    @RequestMapping(value = {"/orders-mine/orders-placement","/m/orders-mine/orders-placement"}, method = RequestMethod.POST)
    public String placeOrderForCartItems(@DateTimeFormat(pattern = "yyyyMMddHHmmsssss") Date timestamp,Long consigneeid, String message,HttpServletRequest request) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);

        boolean operateFlag = ordersService.placeOrderForCartItems(timestamp, consigneeid,message, member, IPAddressResolver.getIpAddress(request));

        if (operateFlag) {
            if (DeviceUtils.getCurrentDevice(request).isNormal()) {
                return "redirect:/orders-mine/buys";
            } else {
                return "redirect:/m/orders-mine/buys";
            }
        } else {
            return "error";
        }
    }

    //前台会员购买订单页面管理页面
    @RequestMapping(value = {"/orders-mine/buys","/m/orders-mine/buys"}, method = RequestMethod.GET)
    public String getOrdersMineBuyPage(@RequestParam(required = false, defaultValue = "1") Integer pageNumber, HttpServletRequest request,Model model) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
        Long memberid = null;
        if (member != null) {
            memberid = member.getMemberid();
        }
        PageModel<Orders> pageModel = ordersService.getOrdersByMemberRole(memberid, null, pageNumber, 2);

        model.addAttribute("pageModel", pageModel);

        return "orders-mine-buys";
    }

    //前台会员购买订单页面管理页面
    @RequestMapping(value = {"/orders-mine/delegates","/m/orders-mine/delegates"}, method = RequestMethod.GET)
    public String getOrdersMineDelegatePage(@RequestParam(required = false, defaultValue = "1") Integer pageNumber, HttpServletRequest request,Model model) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
        Long memberid = null;
        if (member != null) {
            memberid = member.getMemberid();
        }
        PageModel<Orders> pageModel = ordersService.getOrdersByMemberRole(null, memberid, pageNumber, 2);

        model.addAttribute("pageModel", pageModel);

        return "orders-mine-delegates";
    }


    //销售统计页面
    @RequestMapping(value = {"/orders-mine/delegate-statistics"}, method = RequestMethod.GET)
    public String getOrdersMineDelegatesStatisticsByFiltersPage(HttpServletRequest request, Model model) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
        return "delegate-statistics";
    }

    //销售统计页面，json请求结果
    @RequestMapping(value = {"/orders-mine/delegate-statistics"}, method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse getOrdersMineDelegatesStatisticsByFilters(@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date beginTime,
                                                                  @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endTime,
                                                                  @RequestParam(required = false) Long brandid,@RequestParam(required = false) Long categoryid,
                                                                  HttpServletRequest request, Model model) {
        JsonResponse jsonResponse = new JsonResponse();
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        Long memberid = null;
        if (member != null) {
            memberid = member.getMemberid();
        }
        if (memberid == null) {
            jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
        } else {
            PageModel<OrdersShow> pageModel = ordersService.getDelegateOrdersWithFilters(beginTime, endTime, memberid, brandid, categoryid);
            jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
            jsonResponse.setContent(pageModel);
        }
        return jsonResponse;
    }



}
