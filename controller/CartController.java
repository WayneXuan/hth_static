package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.Cart;
import com.hth.lh.persistence.model.Category;
import com.hth.lh.persistence.model.Member;
import com.hth.lh.service.CartService;
import com.hth.lh.service.CategoryService;
import com.hth.lh.web.model.*;
import com.hth.lh.web.util.IPAddressResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/5/15
 * Time: 17:58
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @Autowired
    private CategoryService categoryService;

    //通过json请求返回购物车中的物品json数据
    @RequestMapping(value = {"/cart/items", "/m/cart/items"}, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public JsonResponse getCartItemsWithJson(HttpServletRequest request) {
        JsonResponse jsonResponse = new JsonResponse();

        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        Long memberid = member.getMemberid();
        List<Cart> cartList = cartService.getCartListByMemberid(memberid);
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(cartList);
        return jsonResponse;
    }


    //通过非json请求获得购物车页面
    @RequestMapping(value = {"/cart/items", "/m/cart/items"}, consumes = "!application/json")
    public String getCartItemsWithPage(HttpServletRequest request, Model model) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        String deviceType = DeviceKey.DEVICE_MOBILE;
        if (DeviceUtils.getCurrentDevice(request).isNormal()) {
            deviceType = DeviceKey.DEVICE_PC;
        }
        List<Cart> cartList = cartService.getCartListWithItemDetail(member, deviceType);
        model.addAttribute("cartList", cartList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
        return "cart";
    }


    //向购物车增加、删除、增减物品数量，并且下单；order=true表示立即下单
    @RequestMapping(value = {"/cart/item-all", "/m/cart/item-all"})
    public String addUpdateDeleteCartItemAndPlaceOrder(CartItemForm cartItemForm, @RequestParam(required = false) Boolean order, HttpServletRequest request) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);

        cartService.addUpdateDeleteCartItem(cartItemForm.getCarts(), member, IPAddressResolver.getIpAddress(request));

        if (order == null || order == false) {
            if (DeviceUtils.getCurrentDevice(request).isNormal()) {
                return "redirect:/cart/items";
            } else {
                return "redirect:/m/cart/items";
            }
        } else {
            if (DeviceUtils.getCurrentDevice(request).isNormal()) {
                return "redirect:/orders-mine/orders-placement";
            } else {
                return "redirect:/m/orders-mine/orders-placement";
            }
        }
    }

    //使用json更新或者删除购物车内的物品
    //增减数量和删除物品
    @RequestMapping(value = {"/cart/item-one", "/m/cart/item-one"})
    @ResponseBody
    public JsonResponse updateDeleteCartItem(CartItem cartItem, HttpServletRequest request) {
        JsonResponse jsonResponse = new JsonResponse();
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        List<CartItem> cartItemList = new ArrayList<>();
        cartItemList.add(cartItem);
        cartService.addUpdateDeleteCartItem(cartItemList,member,IPAddressResolver.getIpAddress(request));
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        return jsonResponse;
    }




    //清空现在购物车中的所有物品
    @RequestMapping(value = {"/cart/item-remove-all", "/m/cart/item-remove-all"})
    public String removeAllCartItemWithPage(HttpServletRequest request) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        cartService.clearCartByMember(member);
        if (DeviceUtils.getCurrentDevice(request).isNormal()) {
            return "redirect:/";
        } else {
            return "redirect:/m/";
        }
    }



}
