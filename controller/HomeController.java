package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.Category;
import com.hth.lh.persistence.model.Item;
import com.hth.lh.persistence.model.Member;
import com.hth.lh.service.CartService;
import com.hth.lh.service.CategoryService;
import com.hth.lh.service.ItemService;
import com.hth.lh.web.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 13-11-7
 * Time: 下午9:18
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class HomeController {


    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ItemService itemService;


    @Autowired
    private CartService cartService;

    //pc版本首页
    @RequestMapping(method = RequestMethod.GET,value = {"/"})
    public String index(Model model,HttpServletRequest request) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        model.addAttribute("itemMap",itemService.getIndexPageItemListMap(member,null,DeviceKey.DEVICE_PC));
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));

        return "index";
    }

    //移动版本各首页榜上分类请求json
    @RequestMapping(method = RequestMethod.GET, value = {"/goodies/section-{sectionKey}","/m/goodies/section-{sectionKey}"})
    @ResponseBody
    public JsonResponse getMobileSectionItems(@PathVariable String sectionKey, HttpServletRequest request) {
        JsonResponse jsonResponse = new JsonResponse();
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        jsonResponse.setContent(itemService.getIndexPageItemListMap(member,sectionKey, DeviceKey.DEVICE_MOBILE));
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        return jsonResponse;
    }

    //移动版本首页
    @RequestMapping(method = RequestMethod.GET,value = {"/m/"})
    public String indexMobile(Model model,HttpServletRequest request) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        model.addAttribute("itemMap",itemService.getIndexPageItemListMap(member,"fresh_racking",DeviceKey.DEVICE_PC));

        return "index";
    }





    //pc版本频道页面
    @RequestMapping(value = {"/channel-{categoryid}", "/m/channel-{categoryid}"})
    public String getChannelPageByCategoryid(@PathVariable Long categoryid, HttpServletRequest request,Model model) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        Category category = categoryService.getCategoryMap().get(categoryid);
        if (category != null&&category.getParentCategory()==null) {
            model.addAttribute("category", category);

            Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);

            List<List<ItemShow>> itemShowListArray = itemService.getChildrenCategoryItemListMap(categoryid,10,member);
            model.addAttribute("itemShowListArray", itemShowListArray);
            model.addAttribute("cartListBrief", cartService.getCartListBrief(member));

            return "channel";
        } else {
            return "notfoundpage";
        }

    }

    //手机版本分类选择页面
    @RequestMapping(value = {"/goodies/categories","/m/goodies/categories"})
    public String getMobileCategoriesPage(Model model) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        return "goodies-categories";
    }


    //商品搜索页面
    //搜索过滤器
    //关键字、品牌选择、分类选择、分类属性选项选择
    //排序：销量、价格、上线时间
    //分页页码
    //会员授权信息
    //输出过滤器：
    //关键字、选中分类（如果是子分类，打印父类）、叶子分类（所有物品所属分类，而不是自然分类；如果选中叶子分类不打印）、品牌和品牌选中
    //分类属性和选项选中（如果是叶子分类、打印叶子分类下属性选项和选中属性选项）
    //排序键和顺序orders,rack,price
    //分页页码
    @RequestMapping(value = {"/goodies","/m/goodies"},consumes = {"!application/json"},produces = "text/html")
    public String getGoodiesSearchResultsByFilters(@RequestParam(required = false) String keyword,
                                                   @RequestParam(required = false) Long brandid,
                                                   @RequestParam(required = false) Long categoryid,
                                                   @RequestParam(required = false) List<Long> optionids,
                                                   @RequestParam(required = false,defaultValue = "orders") String orderkey,
                                                   @RequestParam(required = false,defaultValue = "asc") String order,
                                                   @RequestParam(required = false,defaultValue = "1",value = "pagenumber") Integer pageNumber,
                                                   HttpServletRequest request,Model model) {

        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);

        GoodiesFilter goodiesFilter = itemService.getGoodiesSearchResults(keyword, brandid, categoryid, optionids, orderkey, order, pageNumber, 1, member,DeviceKey.DEVICE_PC);
        model.addAttribute("goodiesFilter", goodiesFilter);
        model.addAttribute("pageModel", goodiesFilter.getPageModel());
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));

        return "goodies";
    }

    //商品搜索，纯json请求和返回json数据结果
    @RequestMapping(value = {"/goodies","/m/goodies"},consumes = "application/json",produces = "application/json")
    @ResponseBody
    public JsonResponse getGoodiesSearchResultsByFiltersJson(@RequestParam(required = false) String keyword,
                                                             @RequestParam(required = false) Long brandid,
                                                             @RequestParam(required = false) Long categoryid,
                                                             @RequestParam(required = false) List<Long> optionids,
                                                             @RequestParam(required = false,defaultValue = "orders") String orderkey,
                                                             @RequestParam(required = false,defaultValue = "asc") String order,
                                                             @RequestParam(required = false,defaultValue = "1") Integer pageNumber,
                                                             HttpServletRequest request) {

        JsonResponse jsonResponse = new JsonResponse();
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        GoodiesFilter goodiesFilter = itemService.getGoodiesSearchResults(keyword, brandid, categoryid, optionids, orderkey, order, pageNumber, 20, member,DeviceKey.DEVICE_MOBILE);

        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        jsonResponse.setContent(goodiesFilter);
        return jsonResponse;
    }


    //商品详情
    @RequestMapping(value = {"/goodies/item-{itemid}","/m/goodies/item-{itemid}"})
    public String getItemDetail(@PathVariable Long itemid, Model model,HttpServletRequest request) {
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        Item item = null;
        if (DeviceUtils.getCurrentDevice(request).isNormal()) {
            item = itemService.getItemDetailByItemidMember(itemid, member, DeviceKey.DEVICE_PC);
        } else {
            item = itemService.getItemDetailByItemidMember(itemid, member, DeviceKey.DEVICE_MOBILE);
        }
        if (item != null) {
            model.addAttribute("item", item);
            model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
            return "item";
        }
        return "notfoundpage";
    }


}
