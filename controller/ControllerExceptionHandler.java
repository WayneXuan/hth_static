package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.Member;
import com.hth.lh.web.model.JsonResponse;
import com.hth.lh.web.model.PageErrorKey;
import com.hth.lh.web.model.SessionKey;
import com.hth.lh.web.model.URIKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 13-9-16
 * Time: 下午2:18
 * To change this template use File | Settings | File Templates.
 */

public class ControllerExceptionHandler implements HandlerExceptionResolver {

    private static Logger logger = LoggerFactory.getLogger("controllerException");

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) {

        StringBuilder stringBuilder = new StringBuilder("{");
        Iterator<Map.Entry<String,String[]>> iterator = request.getParameterMap().entrySet().iterator();
        while (iterator.hasNext()) {
            stringBuilder.append("{");
            Map.Entry<String,String[]> entry =  iterator.next();
            stringBuilder.append(entry.getKey());
            stringBuilder.append(":[");
            boolean commaFlag = false;
            for (String value:entry.getValue()) {
                if (commaFlag) {
                    stringBuilder.append(",");
                }
                else {
                    commaFlag=true;
                }
                stringBuilder.append(value);
            }
            stringBuilder.append("]");
            if (iterator.hasNext()) {
                stringBuilder.append(",");
            }
        }

        stringBuilder.append("}");

        Member member = (Member)WebUtils.getSessionAttribute(request,SessionKey.SESSION_MEMBER);
        Long memberid = null;
        if (member!=null) {
            memberid = member.getMemberid();
        }
        logger.error("userid-{},url-{}, params-{}",memberid,request.getRequestURL(),stringBuilder.toString(),exception);
        ModelAndView modelAndView = null;

        //json request exception handler
        if (request.getRequestURI().matches(URIKey.JSON_URI_EXP)||"application/json".equals(request.getContentType())) {
            JsonResponse jsonResponse = new JsonResponse();
            jsonResponse.setStatus(JsonResponse.RESPONSE_EXCEPTION);
            jsonResponse.setErrorMessage((String)WebUtils.getSessionAttribute(request,SessionKey.SESSION_ERROR_MESSAGE));
            modelAndView = new ModelAndView(PageErrorKey.jsonView);
            modelAndView.addObject(PageErrorKey.status,jsonResponse.getStatus());
            modelAndView.addObject(PageErrorKey.errorMessage,jsonResponse.getErrorMessage());
        }
        else {
            //pc端提示页面
            if (DeviceUtils.getCurrentDevice(request).isNormal()) {
                modelAndView = new ModelAndView(PageErrorKey.pageView);
            }
            else {
                //移动端提示页面
                modelAndView = new ModelAndView(PageErrorKey.mobilePageView);
            }
            modelAndView.addObject(PageErrorKey.errorMessage,(String)WebUtils.getSessionAttribute(request,SessionKey.SESSION_ERROR_MESSAGE));
        }

        //bad request internal server error
        response.setStatus(500);


        return modelAndView;
    }

}
