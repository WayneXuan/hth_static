package com.hth.lh.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/4/21
 * Time: 17:17
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class GateController {

    //页面未找到错误指向页面
    @RequestMapping(method = RequestMethod.GET, value = {"/pagenotfound","/m/pagenotfound"})
    public String pagenotfoundPage() {
        return "pagenotfound";
    }



    //内部错误exception指向页面
    @RequestMapping(method = RequestMethod.GET, value = {"/error","/m/error"})
    public String errorPage() {
        return "error";
    }

}
