package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.Category;
import com.hth.lh.persistence.model.Consignee;
import com.hth.lh.persistence.model.Member;
import com.hth.lh.service.CartService;
import com.hth.lh.service.CategoryService;
import com.hth.lh.service.ConsigneeService;
import com.hth.lh.web.model.JsonResponse;
import com.hth.lh.web.model.SessionKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/5/19
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ConsigneeController {


    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CartService cartService;

    @Autowired
    private ConsigneeService consigneeService;

    //移除、设置默认地址
    @RequestMapping(value = {"/consignee/address-config", "/m/consignee/address-config"}, method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse deleteSetdefaultConsignee(@RequestParam Long consigneeid, @RequestParam String operate,HttpServletRequest request) {

        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);

        Long memberid = null;
        if (member != null) {
            memberid = member.getMemberid();
        }

        JsonResponse jsonResponse = new JsonResponse();
        if ("delete".equals(operate)) {
            Consignee consignee = new Consignee();
            consignee.setIsdeleted(true);
            consignee.setConsigneeid(consigneeid);
            consignee.setMemberid(memberid);
            consigneeService.addUpdateDeleteConsignee(consignee);
            jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        } else if ("default".equals(operate)) {
            Consignee consignee = new Consignee();
            consignee.setConsigneeid(consigneeid);
            consignee.setIsdefault(true);
            consignee.setMemberid(memberid);
            consigneeService.addUpdateDeleteConsignee(consignee);
            jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        } else {
            jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
        }

        return jsonResponse;
    }

    //地址簿页面
    @RequestMapping(value = {"/consignee", "/m/consignee"}, method = RequestMethod.GET)
    public String getConsigneePage(HttpServletRequest request,Model model) {

        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
        Long memberid = null;
        if (member != null) {
            memberid = member.getMemberid();
        }

        List<Consignee> consigneeList = consigneeService.getConsigneeListByMemberid(memberid);
        model.addAttribute("consigneeList", consigneeList);
        model.addAttribute("addressProvinceMap", consigneeService.getAddressProvinceMap());
        model.addAttribute("addressCityMap", consigneeService.getAddressCityMap());
        model.addAttribute("addressAreaMap", consigneeService.getAddressAreaMap());

        model.addAttribute("consignee", new Consignee());
        model.addAttribute("addressProvinceList", consigneeService.getAddressProvinceList());


        return "consignee";
    }

    //提交新的收件人地址、更新收件人地址
    @RequestMapping(value = {"/consignee", "/m/consignee"}, method = RequestMethod.POST)
    public String addEditConsigneeAddress(@ModelAttribute @Validated Consignee consignee,BindingResult bindingResult, HttpServletRequest request, Model model) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);

        Boolean addressValid = consigneeService.fillConsigneeAddressWithAreaid(consignee);
        if (addressValid == false) {
            bindingResult.addError(new ObjectError("consignee.areaid","无效的地区编号"));
        }
        if (bindingResult.hasErrors()) {
            List<Category> categoryList = categoryService.getAllCategory();
            model.addAttribute("categoryList", categoryList);
            model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
            Long memberid = null;
            if (member != null) {
                memberid = member.getMemberid();
            }

            List<Consignee> consigneeList = consigneeService.getConsigneeListByMemberid(memberid);
            model.addAttribute("consigneeList", consigneeList);
            model.addAttribute("addressProvinceMap", consigneeService.getAddressProvinceMap());
            model.addAttribute("addressCityMap", consigneeService.getAddressCityMap());
            model.addAttribute("addressAreaMap", consigneeService.getAddressAreaMap());

            model.addAttribute("hasError", true);
            model.addAttribute("addressProvinceList", consigneeService.getAddressProvinceList());
            model.addAttribute("provinceidCityListMap", consigneeService.getProvinceidCityListMap());
            model.addAttribute("cityidAreaListMap", consigneeService.getCityidAreaListMap());

            return "consignee";
        }

        //新增收件人
        Long memberid = null;
        if (member != null) {
            memberid = member.getMemberid();
        }
        consignee.setMemberid(memberid);
        consignee.setConsigneeid(null);

        consigneeService.addUpdateDeleteConsignee(consignee);

        return "redirect:/consignee";

    }

    //通过json请求获得省份的下线城市的列表
    @RequestMapping(value = {"/consignee/province-city-{provinceid}", "/m/consignee/province-city-{provinceid}"}, method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse getProvinceidCityList(@PathVariable Long provinceid) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setContent(consigneeService.getProvinceidCityListMap().get(provinceid.toString()));
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        return jsonResponse;
    }

    //通过json请求获得城市的下线地区的列表
    @RequestMapping(value = {"/consignee/city-area-{cityid}", "/m/consignee/city-area-{cityid}"}, method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse getCityAreaList(@PathVariable Long cityid) {
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setContent(consigneeService.getCityidAreaListMap().get(cityid.toString()));
        jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        return jsonResponse;
    }








}
