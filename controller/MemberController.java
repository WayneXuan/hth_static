package com.hth.lh.web.controller;

import com.hth.lh.persistence.model.Category;
import com.hth.lh.persistence.model.Invitation;
import com.hth.lh.persistence.model.Member;
import com.hth.lh.service.CartService;
import com.hth.lh.service.CategoryService;
import com.hth.lh.service.InvitationService;
import com.hth.lh.service.MemberService;
import com.hth.lh.web.model.CookieKey;
import com.hth.lh.web.model.JsonResponse;
import com.hth.lh.web.model.PageModel;
import com.hth.lh.web.model.SessionKey;
import com.hth.lh.web.util.CookieUtils;
import com.hth.lh.web.util.IPAddressResolver;
import com.hth.lh.web.validation.MemberPassword;
import com.hth.lh.web.validation.MemberRegister;
import com.hth.lh.web.validation.MemberUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/4/23
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class MemberController {

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CartService cartService;


    //注册会员页面
    @RequestMapping(method = RequestMethod.GET, value = {"/members/member-register", "/m/members/member-register"})
    public String registerPage(@RequestParam(required = false, value = "invitationcode") String invitationCode, Model model) {
        model.addAttribute("member", new Member());
        //验证邀请码的存在
        if (invitationCode == null || invitationService.validateInvitationCode(invitationCode)) {
            model.addAttribute("invitationCode", invitationCode);
            return "register";
        } else {
            model.addAttribute("invitationError", true);
            return "register";
        }

    }

    //注册会员资料提交
    @RequestMapping(method = RequestMethod.POST, value = {"/members/member-register", "/m/members/member-register"})
    public String registerSubmit(@RequestParam(value = "invitationcode") String invitationCode,
                                 @ModelAttribute @Validated({MemberRegister.class}) Member member, BindingResult bindingResult, Model model,
                                 HttpServletRequest request) {

        model.addAttribute("invitationCode", invitationCode);

        //验证用户名的有效唯一性
        boolean passportidUnique = memberService.validatePassportidUnique(member.getPassportid());

        //验证邀请信息有效性
        Invitation invitation = invitationService.getEffectiveInvitationByInvitationCode(invitationCode);
        if (invitation == null || bindingResult.hasErrors() || passportidUnique == false) {
            if (invitation == null) {
                model.addAttribute("invitationError", true);
            }
            model.addAttribute("passportidUniqueError", !passportidUnique);
            return "register";

        }
        //用户信息验证通过，注册

        if (memberService.registerMember(member, invitation) != null) {

            //注册成功
            //记录登录时间和Ip
            //注册完成，将member放入session
            member.setLoginTimes(member.getLoginTimes() + 1);
            member.setLastLogin(new Date());
            member.setLastIp(IPAddressResolver.getIpAddress(request));

            memberService.updateMemberInfo(member);
            WebUtils.setSessionAttribute(request, SessionKey.SESSION_MEMBER, member);

            return "redirect:/";

        } else {
            model.addAttribute("registerError", true);
            return "register";
        }

    }

    //会员登录
    @RequestMapping(method = RequestMethod.GET, value = {"/members/member-login", "/m/members/member-login"})
    public String loginPage() {
        return "login";
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/members/member-login", "/m/members/member-login"})
    public String loginSubmit(@RequestParam String passportid, @RequestParam String password,
                              @RequestParam(required = false) Boolean keepalive,Model model, HttpServletRequest request,
                              HttpServletResponse response) {
        Member member = memberService.getMemberByLogin(passportid, password);
        if (member != null && member.getStatus().equals(Member.Status.Normal)) {

            memberService.loginUpdateMemberKeepalive(member, keepalive,IPAddressResolver.getIpAddress(request));
            WebUtils.setSessionAttribute(request, SessionKey.SESSION_MEMBER, member);
            if (keepalive != null && keepalive == true) {
                CookieUtils.addCookie(response,CookieKey.COOKIE_LOGIN,member.getCookieid());
            }
            String last_request_url = (String) WebUtils.getSessionAttribute(request, SessionKey.SESSION_LAST_REQUEST_URL);
            if (last_request_url != null) {
                WebUtils.setSessionAttribute(request, SessionKey.SESSION_LAST_REQUEST_URL, null);
                return "redirect:" + last_request_url;
            } else {
                if (DeviceUtils.getCurrentDevice(request).isNormal()) {
                    return "redirect:/";
                } else {
                    return "redirect:/m/";
                }
            }
        }
        model.addAttribute("loginError", true);
        model.addAttribute("passportid", passportid);
        model.addAttribute("password", password);
        return "login";
    }


    //会员登出
    @RequestMapping({"/members/member-logout", "/m/members/member-logout"})
    public String logout(HttpServletRequest request,HttpServletResponse response) {
        //清空session中的会员信息
        WebUtils.setSessionAttribute(request, SessionKey.SESSION_MEMBER, null);
        //清空cookie
        CookieUtils.cleanCookie(response, CookieKey.COOKIE_LOGIN);
        return "login";
    }


    @RequestMapping(method = {RequestMethod.GET}, value = {"/members"})
    public String getMemberList(HttpServletRequest request,
                                @RequestParam(required = false) String passportid,
                                @RequestParam(required = false, value = "begintime") @DateTimeFormat(pattern = "yyyy-MM-dd") Date beginTime,
                                @RequestParam(required = false, value = "endtime") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endTime,
                                @RequestParam(required = false, defaultValue = "1", value = "pagenumber") Integer pageNumber,
                                Model model) {


        PageModel<Member> pageModel = memberService.getMembersByFilters(passportid, beginTime, endTime, pageNumber, 30);
        model.addAttribute("pageModel", pageModel);
        return "members";
    }


    //禁用会员账号
    @RequestMapping(method = {RequestMethod.POST}, value = {"/members/member-ban-{memberid}"}, params = {"banflag"},
            produces = "application/json")
    @ResponseBody
    public JsonResponse banMember(@PathVariable Long memberid,
                                  @RequestParam(value = "banflag") Boolean banFlag) {

        JsonResponse jsonResponse = new JsonResponse();

        boolean result = memberService.banMember(memberid, banFlag);
        if (result) {
            jsonResponse.setStatus(JsonResponse.RESPONSE_SUCCESS);
        } else {
            jsonResponse.setStatus(JsonResponse.RESPONSE_NOT_SUCCESS);
        }

        return jsonResponse;

    }

    //会员自己的信息页面
    @RequestMapping(method = {RequestMethod.GET}, value = {"/member-mine", "/m/member-mine"})
    public String getMemberMineDetailPage(@RequestParam(required = false) Boolean updated, HttpServletRequest request, Model model) {

        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        model.addAttribute("member", member);
        if (updated != null) {
            model.addAttribute("updated", true);
        }

        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));
        return "member-mine";
    }

    //修改会员一般信息提交
    @RequestMapping(method = {RequestMethod.POST}, value = {"/member-mine/member-info", "/m/member-mine/member-info"})
    public String updateMemberInfo(@ModelAttribute @Validated({MemberUpdate.class}) Member memberUpdated, BindingResult bindingResult, HttpServletRequest request, Model model) {
        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));


        if (bindingResult.hasErrors()) {
            model.addAttribute("memberUpdated", memberUpdated);
            return "member-mine-info";
        }

        if (memberService.updateMemberInfoPassword(member, memberUpdated,null)) {
            return "redirect:/member-mine?updated=true";
        } else {
            return "redirect:/member-mine?updated=false";
        }

    }

    //修改会员密码提交
    @RequestMapping(method = {RequestMethod.POST}, value = {"/member-mine/member-password", "/m/member-mine/member-password"})
    public String updateMemberPassword(@RequestParam String orlpassword, @Validated({MemberPassword.class}) Member memberUpdated, BindingResult bindingResult, HttpServletRequest request, Model model) {

        Member member = (Member) WebUtils.getSessionAttribute(request, SessionKey.SESSION_MEMBER);
        List<Category> categoryList = categoryService.getAllCategory();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("cartListBrief", cartService.getCartListBrief(member));

        model.addAttribute("memberUpdated", memberUpdated);

        if (bindingResult.hasErrors() || orlpassword == null || "".equals(orlpassword)) {
            return "member-mine-password";
        }

        if (!memberService.updateMemberInfoPassword(member, memberUpdated, orlpassword)) {
            model.addAttribute("passwordWrong", true);
            return "member-mine-password";
        }


        return "redirect:/member-mine?updated=true";

    }






}
