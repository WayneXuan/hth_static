/**
 * Created by pcliang on 2014/6/9.
 */
//定义列表操作类
function  List(listId) {
    this.listId = listId;   //公共变量
}

List.categories=null;            // 静态变量：
List.selectedCate=null;
List.selectedCate_raw=null;
List.selectedSubcate=null;
List.selectedSubcate_raw=null;
List.selectedProp=null;
List.selectedProp_raw=null;
List.selectedOption=null;
List.selectedOption_raw=null;
List.selectedBrand=null;

List.prototype.getlistId = function(){
    return  this.listId;
};

var validate = function() {
    //获取表单值，非法的不能提交
    if ( !$.isNumeric($("#price input").val()) ) {
        jAlert("请输入正确的价格！");
        return false;
    }
    if ($("#itemname input").val()=="") {
        jAlert("请输入物品名称！");
        return false;
    }
    if ($("#itemdesc textarea").val()=="") {
        jAlert("请输入物品描述！");
        return false;
    }
    return true;
};

var loaddefault = function(){
    $.ajax({
        url: '/categories/category-tree',
        type: "GET",
        success: function(data) {
            //*********************************************************************//
            //获取树数据
            //*********************************************************************//
            if(data.status==0){
                //存储categories Json数据到List.categories
                List.categories = data.content;
//            初始化分类属性和品牌数据
                $("select option").remove();
                $.each(List.categories,function(k,v){
                    $("#cates select").append("<option value='"+ v.categoryid+"'>"+ v.categoryName+"</option>");
                    if(k==0){
                        List.selectedCate_raw = v;
                    }
                });
                List.selectedCate = $('#cates select').val();

                $.each(List.selectedCate_raw.childrenCategory,function(k,v){
                    $("#subcates select").append("<option value='"+ v.categoryid+"'>"+ v.categoryName+"</option>");
                    if(k==0){
                        List.selectedSubcate_raw = v;
                    }
                });
                List.selectedSubcate = $('#subcates select').val();
                //显示默认属性
                $.each(List.selectedSubcate_raw.categoryPropertySet,function(k,v){
                    $("#propopts .popairs").append("<label value='"+ v.propertyid+"'>"+ v.propertyName+"：</label><br>");
                });

                //对默认显示的属性附加选项
                var vs = List.selectedSubcate_raw;
                var len = vs.categoryPropertySet.length;
                $.each(vs.categoryPropertySet,function(i,v){
                    $.each(v.propertyOptionSet,function(io,vo){
                        $("#propopts .popairs "+"label[value="+v.propertyid+"]").append("<input type='checkbox' value='" + vo.optionid + "' name='optionids'" +" group='" + v.propertyName +"'/>"+ vo.optionName);

                    });
                    if (i==len-1){
                        $("#propopts").append("<br>");
                    }
                });

                //将选项checkbox改成单选
                $('label input[type=checkbox]').each(function(){
                    $(this).click(function(){
//                    console.log(this.getAttribute('group'));
                        $("label input[group="+this.getAttribute('group')+"]").each(function(k,v){//将该属性的所有选项重置
                            v.checked = false;
                        });
                        this.checked = this.checked?false:true;
                    });
                });
                $.each(List.categories[0].childrenCategory[0].brandCategoryMapSet,function(k,v){
                    $("#brands select").append("<option value='"+ v.brandid+"'>"+ v.brand.brandName+"</option>");
                });
                List.selectedBrand = $('#brands select').val();
            }
        }
    });
};


//获取分类
$(document).ready(function() {
    loaddefault();

    //uploadify
    $("#avatarpicture").uploadify({
        swf:'/js/uploadify.swf',
        uploader:'/items/picture-avatar-new',
        cancelImg: '/js/uploadify-cancel.png',
        fileObjName:'avatarpicture',
        fileTypeExts:'*.jpg;*.jpeg;',
        fileSizeLimit:'10MB',
        multi:false,
        formData: {
            picturetype: 'avatar'
        },
            //上传到服务器，服务器返回相应信息到data里
        onUploadSuccess:function(file, data, response){
            var obj = $.parseJSON(data);
            if(obj.status==0){
                $("#avatar_list").append("<img width='40' height='60' src='"+"/img"+obj.content.filePath+"_p.jpg' id='"+obj.content.pictureid+ "'/>");
                $("#avatarPictureids").val(obj.content.pictureid);
                $("#avatar_list img").bind("click",function(){
                    //获取pictureid
                    var pictureid = this.id;
                    $.ajax({
                        url: '/items/picture-remove-'+pictureid,
                        type: "POST",
                        success: function (data) {
//                              console.log('deleted!');
                            //remove element
                            $("#"+pictureid).remove();
                            $("[value='"+pictureid+"']").remove();//删除隐含表单值
                        }
                    });
                });
            } else{
                jAlert("上传失败，请重试。");
            }

        }
    });
    $("#carouselspicture").uploadify({
        swf:'/js/uploadify.swf',
        uploader:'/items/picture-carousels-new',
        cancelImg: '/js/uploadify-cancel.png',
        fileObjName:'avatarpicture',
        fileTypeExts:'*.jpg;*.jpeg;',
        fileSizeLimit:'10MB',
        multi:false,
        formData: {
            picturetype: 'carousels'
        },
        //上传到服务器，服务器返回相应信息到data里
        onUploadSuccess:function(file, data, response){
            console.log(data);
            var obj = $.parseJSON(data);
            if(obj.status==0){
                $("#carousels_list").append("<img width='40' height='60' src='/img"+obj.content.filePath+"_p.jpg' id='"+obj.content.pictureid+ "'/>");

                $("#hidden_carousels").append("<input type='hidden' name='carouselsPictureids' value='"+obj.content.pictureid+"'/>");
                //点击删除跑马灯图片
                $("#carousels_list img").bind("click",function(){
                    //获取pictureid
                    var pictureid = this.id;
                    $.ajax({
                        url: '/items/picture-remove-'+this.id,
                        type: "POST",
                        success: function (data) {
//                              console.log('deleted!');
                            //remove element
                            $("#"+pictureid).remove();
                            $("[value='"+pictureid+"']").remove();//删除隐含表单值
                        }
                    });
                });

            } else{
                jAlert("上传失败，请重试。");
            }
        }
    });



    $('#cates select').change(function () {
//       更新子分类和品牌，清空属性列表
        $("#subcates select option").remove();
        $("#brands select option").remove();

        $("#propopts .popairs label").remove();
        $("#propopts br").remove();
        //获取选中categoryid
        List.selectedCate = $('#cates select').val();

        //查找该categoryid下的所有子分类和品牌并添加
        $.each(List.categories,function(k,v){
           if (v.categoryid==List.selectedCate){
               List.selectedCate_raw = v;
               //若该分类下没有子分类则添加该分类的属性
               if(v.childrenCategory.length==0){
                   $.each(v.categoryPropertySet, function (kp,vp) {
                       $("#propopts .popairs").append("<label value='"+ vp.propertyid+"'>"+ vp.propertyName+"：</label><br>");
                   });
               }
               $.each(v.childrenCategory,function(ks,vs){
                   $("#subcates select").append("<option value='"+ vs.categoryid+"'>"+ vs.categoryName+"</option>");
               });
               //获取默认子分类id，添加属性
               List.selectedSubcate = $('#subcates select').val();
               $.each(v.childrenCategory,function(key,value){
                    if(value.categoryid== List.selectedSubcate) {
                        $.each(value.categoryPropertySet,function(kp,vp){
                            $("#propopts .popairs").append("<label value='"+ vp.propertyid+"'>"+ vp.propertyName+"：</label><br>");
                        });
                    }
               });

               //获取默认属性，添加选项
               List.selectedProp = $('#props select').val();
               if(v.childrenCategory.length==0){//若没有子类则直接添加属性的选项
                   var len = v.categoryPropertySet.length;
                   $.each(v.categoryPropertySet,function(kp,vp){
                       $.each(vp.propertyOptionSet,function(io,vo){
                           $("#propopts .popairs "+"label[value="+vp.propertyid+"]").append("<input type='checkbox' value='"+vo.optionid+"' name='optionids'"+" group='"+v.propertyName+"'/>"+ vo.optionName);
                       });
                       if (kp==len-1){
                           $("#propopts").append("<br>");
                       }
                   });
               }else{//若有子类则添加默认子类的属性的选项
                   $.each(v.childrenCategory,function(key,value){
                       if(value.categoryid== List.selectedSubcate) {
                           var len = value.categoryPropertySet.length;
                           $.each(value.categoryPropertySet,function(i,v){
                               $.each(v.propertyOptionSet,function(io,vo){
                                   $("#propopts .popairs "+"label[value="+v.propertyid+"]").append("<input type='checkbox' value='" + vo.optionid + "' name='optionids'"+" group='"+v.propertyName+"'/>"+ vo.optionName);
                               });
                               if (i==len-1){
                                   $("#propopts").append("<br>");
                               }
                           });
                       }
                   });
               }
               //将选项checkbox改成单选
               $('label input[type=checkbox]').each(function(){
                   $(this).click(function(){
//                    console.log(this.getAttribute('group'));
                       $("label input[group="+this.getAttribute('group')+"]").each(function(k,v){//将该属性的所有选项重置
                           v.checked = false;
                       });
                       this.checked = this.checked?false:true;
                   });
               });

               List.selectedOption = $('#options select').val();

               $.each(v.brandCategoryMapSet,function(ks,vs){
                   $("#brands select").append("<option value='"+ vs.brandid+"'>"+ vs.brand.brandName+"</option>");
               });
               List.selectedBrand = $('#brands select').val();
           }
        });

    });
    $('#subcates select').change(function () {
//       更新属性列表和品牌列表
//        $("#props select option").remove();
        $("#brands select option").remove();
//        $("#options select option").remove();

        $("#propopts .popairs label").remove();
        $("#propopts br").remove();
        //获取选中categoryid
        List.selectedSubcate = $('#subcates select').val();

        //查找该categoryid子分类的属性和品牌并添加
        $.each(List.selectedCate_raw.childrenCategory,function(ks,vs){
            if(vs.categoryid==List.selectedSubcate){
                //更新属性
                $.each(vs.categoryPropertySet, function (kp,vp) {
//                    $("#props select").append("<option value='"+ vp.propertyid+"'>"+ vp.propertyName+"</option>");
                    $("#propopts .popairs").append("<label value='"+ vp.propertyid+"'>"+ vp.propertyName+"：</label><br>");
                });
                List.selectedSubcate_raw = vs;
                //更新选项
//                console.log(vs);
                var len = vs.categoryPropertySet.length;
                $.each(vs.categoryPropertySet,function(i,v){
                    $.each(v.propertyOptionSet,function(io,vo){
                        $("#propopts .popairs "+"label[value="+v.propertyid+"]").append("<input type='checkbox' value='"+vo.optionid+"' name='optionids'"+" group='"+v.propertyName+"'/>"+ vo.optionName);
                    });
                    if (i==len-1){
                        $("#propopts").append("<br>");
                    }
                });
                //将选项checkbox改成单选
                $('label input[type=checkbox]').each(function(){
                    $(this).click(function(){
//                    console.log(this.getAttribute('group'));
                        $("label input[group="+this.getAttribute('group')+"]").each(function(k,v){//将该属性的所有选项重置
                            v.checked = false;
                        });
                        this.checked = this.checked?false:true;
                    });
                });
                $.each(vs.brandCategoryMapSet,function(kb,vb){
                    $("#brands select").append("<option value='"+ vb.brandid+"'>"+ vb.brand.brandName+"</option>");
                });
                List.selectedBrand = $('#brands select').val();
            }

        });
    });

    $('#brands select').change(function(){
        //获取选中品牌id
        List.selectedBrand= $('#brands select').val();
    });

});