/**
 * Created by pcliang on 2014/6/9.
 */
//定义列表操作类
function  List(listId) {
    this.listId = listId;   //公共变量
}

List.categories=null;            // 静态变量：
List.selectedCate=null;
List.selectedCate_raw=null;
List.selectedSubcate=null;
List.selectedSubcate_raw=null;
List.selectedProp=null;
List.selectedProp_raw=null;
List.selectedOption=null;
List.selectedOption_raw=null;
List.selectedBrand=null;

List.prototype.getlistId = function(){
    return  this.listId;
};

//var validate = function () {
//     //获取表单值，非法的不能提交
//    if ( $.isNumeric($("#price input").val()) ) return false;
//    if ($("#itemname input").val()=="") return false;
//    if ($("#itemdesc textarea").val()=="") return false;
//};
var loaddefault = function(){
    $.ajax({
        url: '/categories/category-tree',
        type: "GET",
        success: function(data) {
            //*********************************************************************//
            //获取树数据
            //*********************************************************************//
            if(data.status==0){
                //存储categories Json数据到List.categories
                List.categories = data.content;
//            初始化分类属性和品牌数据
                $("#filters select option").remove();
                $.each(List.categories,function(k,v){
                    if(k==0){//默认分类id值
//                        List.selectedCate_raw = v;
                        $("#cates select").append("<option value='      '>"+ "      "+"</option>");
                    }
                    $("#cates select").append("<option value='"+ v.categoryid+"'>"+ v.categoryName+"</option>");

                });
                List.selectedCate = $('#cates select').val();
            }
        }
    });
};


$(document).ready(function() {
    //加载商品分类品牌属性等信息
    loaddefault();

    $('#cates select').change(function () {
//       更新子分类和品牌，清空属性列表
        $("#brands select option").remove();

        //获取选中categoryid
        List.selectedCate = $('#cates select').val();

        //查找该categoryid下的所有子分类和品牌并添加
        $.each(List.categories,function(k,v){
            if (v.categoryid==List.selectedCate){
                List.selectedCate_raw = v;

                $.each(v.brandCategoryMapSet,function(ks,vs){
                    $("#brands select").append("<option value='"+ vs.brandid+"'>"+ vs.brand.brandName+"</option>");
                });
                List.selectedBrand = $('#brands select').val();
            }
        });

    });

    $('#brands select').change(function(){
        //获取选中品牌id
        List.selectedBrand= $('#brands select').val();
    });

});