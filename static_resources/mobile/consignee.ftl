<#import "/spring.ftl" as spring />
<script type="application/javascript" src="/m/js/jquery-2.1.0.min.js"></script>
<script type="application/javascript">
    $(function() {
        function setDefault()  {$("a[name^='default_']").each(function () {
            $(this).click(function(){
                var consigneeid = $(this).attr("name").substring(8);
                var data_send = {"consigneeid":consigneeid,"operate":"default"};


                $.ajax({
                    url:"/m/consignee/address-config",
                    type:"post",
                    data:data_send,
                    dataType: "json",
                    success:function(data) {
                        if(data.status==0) {
                            $("td[name^='default_']").each(function () {
                                var currentConsigneeid = $(this).attr("name").substring(8);
                                if(currentConsigneeid!=consigneeid) {
                                    $(this).html('<a name="default_'+currentConsigneeid+'" href="javascript:void(0)">设置默认地址</a>');
                                }
                                else {
                                    $(this).html('这是默认地址');
                                }
                            });
                            setDefault();
                        }
                    }
                });

            });
        });}
        setDefault();

        $("a[name^='remove_']").each(function () {
            $(this).click(function(){
                var consigneeid = $(this).attr("name").substring(7);
                var data_send = {"consigneeid":consigneeid,"operate":"delete"};


                $.ajax({
                    url:"/m/consignee/address-config",
                    type:"post",
                    data:data_send,
                    dataType: "json",
                    success:function(data) {
                        if (data.status == 0) {
                            $("#row_" + consigneeid).remove();
                        }
                    }
                });

            });
        });

        $("#new_consignee").click(function () {
            $("#consignee_form").show();
        });

        $("select[name='provinceid']").change(function () {
            if($(this).val()=="") {
                $("select[name='cityid']").empty();
            }
            else{
                $.ajax({
                    url:"/m/consignee/province-city-"+$(this).val(),
                    type:"post",
                    data:null,
                    dataType: "json",
                    success:function(data) {
                        if (data.status == 0) {
                            $("select[name='cityid']").empty();
                            $("select[name='areaid']").empty();
                            $("select[name='cityid']").append('<option>请选择</option>');
                            for(var index=0; index<data.content.length; index++){
                                var city = data.content[index];
                                $("select[name='cityid']").append('<option value="'+city.id+'">'+city.cityName+'</option>')
                            }
                        }
                    }
                });
            }

        });
        $("select[name='cityid']").change(function () {
            if($(this).val()=="") {
                $("select[name='areaid']").empty();
            }
            else{
                $.ajax({
                    url:"/consignee/city-area-"+$(this).val(),
                    type:"post",
                    data:null,
                    dataType: "json",
                    success:function(data) {
                        if (data.status == 0) {
                            $("select[name='areaid']").empty();
                            for(var index=0; index<data.content.length; index++){
                                var area = data.content[index];
                                $("select[name='areaid']").append('<option value="'+area.id+'">'+area.areaName+'</option>')
                            }

                        }
                    }
                });
            }

        });
    });
</script>


<div>
    <p><button id="new_consignee" >新增地址</button></p>
    <p>现有地址</p>
    <div>
        <table>
            <thead>
            <tr>
                <th>收件人</th><th>市区县</th><th>地址</th><th>邮编</th><th>手机号码</th><th>默认</th><th>操作</th>
            </tr>
            </thead>
            <tbody>
            <#if consigneeList??>
                <#list consigneeList as consignee>
                <tr id="row_${consignee.consigneeid}">
                    <td>${consignee.consigneeName!}</td>
                    <td>${(addressProvinceMap[consignee.provinceid?string].provinceName)!}
                        -${(addressCityMap[consignee.cityid?string].cityName)!}
                        -${(addressAreaMap[consignee.areaid?string].areaName)!}</td>
                    <td>${consignee.consigneeAddress!}</td>
                    <td>${consignee.postcode!}</td>
                    <td>${consignee.consigneePhone!}</td>
                    <td name="default_${consignee.consigneeid}"><#if consignee.isdefault??&&consignee.isdefault==true>这是默认地址<#else><a name="default_${consignee.consigneeid}" href="javascript:void(0)">设置默认地址</a> </#if></td>
                    <td><a name="remove_${consignee.consigneeid}" href="javascript:void(0)">删除</a></td>
                </tr>
                </#list>
            </#if>
            </tbody>
        </table>
    </div>
</div>

<div id="consignee_form" <#if hasError??&&hasError==true><#else >style="display: none"</#if>>
    <label id="operation_label"></label>
    <form action="/m/consignee" method="post">
        <input type="hidden" name="consigneeid" value=""/>
        <p>客户名称*：<input type="text" name="consigneeName" value="${(consignee.consigneeName)!}"/>
        <@spring.bind "consignee.consigneeName" />
        <@spring.showErrors "<br/>"/> </p>
        <p>地区*：<select name="provinceid">
            <option value="">请选择</option>
            <#list addressProvinceList as province>
                <option value="${province.id}"
                    <#if consignee.provinceid??&&consignee.provinceid==province.id>selected="selected" </#if>>${province.provinceName!}</option>
            </#list>
        </select>
            <select name="cityid">
                <#if consignee.provinceid??&&provinceidCityListMap??&&provinceidCityListMap[consignee.provinceid?string]??>
                    <#list provinceidCityListMap[consignee.provinceid?string] as city>
                        <option value="${city.id}" <#if consignee.cityid??&&consignee.cityid==city.id>selected="selected" </#if>>${city.cityName!}</option>
                    </#list>
                </#if>
            </select>
            <select name="areaid">
            <#if consignee.cityid??&&cityidAreaListMap??&&cityidAreaListMap[consignee.cityid?string]??>
                <#list cityidAreaListMap[consignee.cityid?string] as area>
                    <option value="${area.id}" <#if consignee.areaid??&&consignee.areaid==area.id>selected="selected" </#if>>${area.areaName!}</option>
                </#list>
            </#if>
            </select>
        <@spring.bind "consignee.areaid" />
        <@spring.showErrors "<br/>"/> </p>
        <p>详细地址：<input type="text" name="consigneeAddress" value="${(consignee.consigneeAddress)!}"/>
        <@spring.bind "consignee.consigneeAddress" />
        <@spring.showErrors "<br/>"/> </p>
        <p>邮编：<input type="text" name="postcode" value="${(consignee.postcode)!}"/>
        <@spring.bind "consignee.postcode" />
        <@spring.showErrors "<br/>"/> </p>
        <p>手机号码*：<input type="text" name="consigneePhone" value="${(consignee.consigneePhone)!}"/>
        <@spring.bind "consignee.consigneePhone" />
        <@spring.showErrors "<br/>"/> </p>
        <p>设置为默认地址：<input type="checkbox" name="isdefault" <#if consignee??&&consignee.isdefault??&&consignee.isdefault==true>checked="checked" </#if>/></p>
        <p><input type="submit" value="提交"/></p>

    </form>
</div>