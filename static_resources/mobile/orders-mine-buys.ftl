<div id="list">
    <table>
        <thead>
        <tr>
            <th>订单号</th>
            <th>价格</th>
            <th>下单日期</th>
            <th>订单状态</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <#if pageModel??&&pageModel.pageList??>
            <#list pageModel.pageList as orders>
            <tr>
                <td><a href="/m/orders-mine/order-${orders.ordersSerialid}">${orders.ordersSerialid!}</a></td>
                <td>${orders.totalPrice!}</td>
                <td>${orders.orderTime?datetime!}</td>
                <td>
                    <#if orders.status??&&orders.status=='Submitted'>已下单<#elseif orders.status??&&orders.status=='Paid'>已付款<#elseif orders.status??&&orders.status=='Discarded'>已作废</#if>
                </td>
                <td>
                    <a href="/m/orders-mine/order-${orders.ordersSerialid}" >查看</a>
                </td>

            </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>

