mobile index page
<div>
    <div id="search_form">
        <form action="/m/merchandises" method="get">
            <input type="text" value="">
            <input type="submit" value="提交">
        </form>
    </div>
    <div><a href="/m/goodies/categories">分类</a><a href="/m/cart/items">购物车</a><a href="/m/orders-mine/buys">我的</a></div>
    <div>
        <a href="/m/goodies/section-fresh_racking">最新上架</a>
        <a href="/m/goodies/section-last_week">上周</a>
        <a href="/m/goodies/section-replenishment">补货</a>
        <a href="/m/goodies/section-recommend">推荐单品</a>
        <a href="/m/goodies/section-exclusive">独家发售</a>
        <a href="/m/goodies/section-hot_sale">热销榜单</a>
        <a href="/m/goodies/section-fashionable">风尚必备</a>
    </div>
</div>
<div>
    <div id="fresh_racking">
    <#if itemMap['fresh_racking']??>
        <#list itemMap['fresh_racking'] as item>
            <div>
                <#if item.avatarPath??>
                    <img src="${item.avatarPath}"/>
                </#if>
            ${item.itemName}
            ${item.price}
            </div>
        </#list>
    </#if>
    </div>
</div>