<div id="status">
    订单：${(orders.ordersSerialid)!}&nbsp;&nbsp;订单状态：
<#if orders.status??&&orders.status=='Submitted'>已提交<#elseif orders.status??&&orders.status=='Paid'>已付款<#elseif orders.status??&&orders.status=='Discarded'>已作废</#if>
</div>
<div id="orders_detail">
    <p>订单详细信息</p>
<#if orders.ordersConsignee??>
    <div>
        <p>送货地址</p>
        <p>${orders.ordersConsignee.consigneeName!}，${orders.ordersConsignee.consigneeAddress!}
            ，${orders.ordersConsignee.postcode!}，
        ${(addressProvinceMap[orders.ordersConsignee.provinceid?string].provinceName)!}
            &nbsp;${(addressCityMap[orders.ordersConsignee.cityid?string].cityName)!}
            &nbsp;${(addressAreaMap[orders.ordersConsignee.areaid?string].areaName)!}
            ，${orders.ordersConsignee.consigneePhone!}
        </p>
    </div>
</#if>

    <div>
        <p>商品详细信息</p>
        <table>
            <thead>
            <tr><th>商品信息</th><th>商品价格</th><th>数量</th><th>总计</th></tr>
            </thead>
            <tbody>
            <#if orders.ordersItemMapSet??>
                <#list orders.ordersItemMapSet as ordersItemMap>
                    <#if ordersItemMap.item??>
                    <tr>
                        <td>
                            <#if ordersItemMap.item.avatarPath??>
                                <img src="/m/img/${ordersItemMap.item.avatarPath}_p.jpg"/>
                            </#if>
                            <label>${ordersItemMap.item.itemName!}</label>
                            <#if ordersItemMap.item.propertyOptionList??>
                                <#list ordersItemMap.item.propertyOptionList as option>
                                ${(option.categoryProperty.propertyName)!}：${(option.optionName)!}
                                </#list>
                            </#if>
                        </td>
                        <td>
                        ${ordersItemMap.discountPrice!},${ordersItemMap.discount!}折
                        </td>
                        <td>
                        ${ordersItemMap.count!}
                        </td>
                        <td>
                        ${(ordersItemMap.discountPrice?double*ordersItemMap.count?int)!}
                        </td>

                    </tr>
                    </#if>
                </#list>
            </#if>
            </tbody>
        </table>
    </div>
    <div>
        <div>留言：${orders.message!}</div>
        <div>总计：${orders.totalPrice!}</div>
    </div>
</div>