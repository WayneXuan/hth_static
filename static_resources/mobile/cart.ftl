<script type="text/javascript" src="/m/js/jquery-2.1.0.min.js"></script>
<script type="application/javascript">
    $(function() {
        $("a[name^='remove_']").each(function () {
            var index = $(this).attr("name").substr(7);
            $(this).click(function () {
                $("#status_" + index).val("del");
                $("#row_" + index).hide();
            });
        });
    });
</script>
<div>
    <form action="/m/cart/item-all" method="post">

    <table>
        <thead>
        <tr><th>商品</th><th>商品价格</th><th>数量</th><th>总计</th><th>更多操作</th></tr>
        </thead>
        <tbody>
        <#if cartList??>
            <#list cartList as cart>
            <tr id="row_${cart_index}">
                <td>
                    <input id="item_${cart.itemid}" type="hidden" name="carts[${cart_index}].itemid" value="${cart.itemid}"/>

                    <#if cart.itemShow.avatarPath??>
                        <img src="${cart.itemShow.avatarPath}"/>
                    </#if>
                    <label>${cart.itemShow.itemName!}</label>
                    <#if cart.itemShow.propertyOptionList??>
                        <#list cart.itemShow.propertyOptionList as option>
                            ${(option.categoryProperty.propertyName)!}：${(option.optionName)!}
                        </#list>
                    </#if>

                </td>
                <td>
                    ${cart.itemShow.price!}
                </td>
                <td>
                    <input name="carts[${cart_index}].count" value="${cart.count}"/>
                </td>
                <td>
                    ${cart.itemShow.price?double*cart.count?int}
                </td>
                <td>
                    <input type="hidden" id="status_${cart_index}" name="carts[${cart_index}].status" value="update"/>
                    <a name="remove_${cart_index}" href="javascript:void(0)">移除</a>
                </td>
            </tr>
            </#list>
        </#if>
        </tbody>
    </table>
    <div><a hidden="/m/cart/item-remove-all">移除所有商品</a></div>
    <div>
        <input type="hidden" name="order" value="true">
            <input type="submit" value="提交"/></div>
    </form>
</div>