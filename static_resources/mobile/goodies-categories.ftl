<div id="search_form">
    <form action="/m/merchandises" method="get">
        <input type="text" value="">
        <input type="submit" value="提交">
    </form>
</div>

<div id="categories">
    <#if categoryList??>
    <ul>
        <#list categoryList as category>
            <li>
                <a href="">${category.categoryName!}</a>
                <#if category.childrenCategory??&&category.childrenCategory?size gt 0>
                    <ul>
                    <#list category.childrenCategory as childCategory>
                        <li><a href="">${childCategory.categoryName!}</a></li>
                    </#list>
                    </ul>
                </#if>
            </li>
        </#list>
    </ul>
    </#if>
</div>