<#import "/spring.ftl" as spring />

<div id="member_info">
    <form action="/m/member-mine/member-info" method="post">

        <p>用户名：${member.passportid!}</p>
        <p>姓名：<input type="text" name="name" value="${memberUpdated.name!}"/>
        <@spring.bind "member.name" />
        <@spring.showErrors "<br/>"/> </p>
        <p>联系方式：<input type="text" name="mobile" value="${memberUpdated.mobile!}"/>
        <@spring.bind "member.mobile" />
        <@spring.showErrors "<br/>"/></p>
        <p>邮箱：<input type="text" name="mail" value="${memberUpdated.mail!}"/>
        <@spring.bind "member.mail" />
        <@spring.showErrors "<br/>"/></p>
        <input type="submit" value="更新"/>
    </form>
    <form action="/m/member-mine/member-password" method="post">
        <p>旧密码：<input type="password" name="orlpassword" value=""/></p>
        <p>新密码：<input type="password" name="password"/></p>
        <p><input type="submit" value="更改"/></p>
    </form>
</div>