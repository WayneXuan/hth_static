<div id="member_info">
    <#if updated??&&updated==true><label>更新成功</label><#elseif updated??&&updated==false><label>更新失败</label></#if>
    <form action="/m/member-mine/member-info" method="post">
        <p>用户名：${member.passportid!}</p>
        <p>姓名：<input type="text" name="name" value="${member.name!}"/></p>
        <p>联系方式：<input type="text" name="mobile" value="${member.mobile!}"/></p>
        <p>邮箱：<input type="text" name="mail" value="${member.mail!}"/></p>
        <input type="submit" value="更新"/>
    </form>
    <form action="/m/member-mine/member-password" method="post">
        <p>旧密码：<input type="password" name="orlpassword" value=""/></p>
        <p>新密码：<input type="password" name="password"/></p>
        <p><input type="submit" value="更改"/></p>
    </form>
</div>