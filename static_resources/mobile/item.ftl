<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <script type="application/javascript" src="/m/js/jquery-2.1.0.min.js"></script>
    <script type="application/javascript">
        $(function() {
            $("form").submit(function () {
                $("#add_count").val($("#count_input").val());
                $("#order_count").val($("#count_input").val());
                return true;
            });
        });
    </script>
</head>
<div id="body">
    <div>商品详情</div>
    <div id="carousels">
    <#if item.carouselsPictureList??>
        <#list item.carouselsPictureList as carouselsPicture>
            <#if carouselsPicture.filePath??>
                <img src="/m/img${carouselsPicture.filePath!}_m.jpg"/>
            </#if>
        </#list>
    </#if>
    </div>
    <div id="content">
        <div>${item.itemName}</div>
        <div>原价：${item.price!}，折扣价：${item.discountPrice!}</div>
    </div>
    <div id="item_buy">
    <#if item.status??>
        <#if item.status=='OnSale'>
            数量：<input type="text" id="count_input" value="1">
            <form action="/m/cart/item-all" method="post">
                <input type="hidden" name="carts[0].itemid" value="${item.itemid}"/>
                <input id="add_count" type="hidden" name="carts[0].count" value="1"/>
                <input type="hidden" name="carts[0].status" value="add"/>
                <input type="submit" value="加入购物车">
            </form>
            <form action="/m/cart/item-all" method="post">
                <input type="hidden" name="carts[0].itemid" value="${item.itemid}"/>
                <input id="order_count" type="hidden" name="carts[0].count" value="1"/>
                <input type="hidden" name="carts[0].status" value="add"/>
                <input type="hidden" name="order" value="true"/>
                <input type="submit" value="立即购买">
            </form>
        </#if>
        <#if item.status=='Replenishment'>
            <div>缺货</div>
        </#if>
    </#if>
    </div>
    <div id="item_description">
        <label>产品描述：</label>
        <div>${item.description!}</div>
    </div>
</div>