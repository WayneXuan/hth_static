<#import "/spring.ftl" as spring />

<div id="member_info">
    <form action="/m/member-mine/member-info" method="post">
        <p>用户名：${member.passportid!}</p>
        <p>姓名：<input type="text" name="name" value="${member.name!}"/> </p>
        <p>联系方式：<input type="text" name="mobile" value="${member.mobile!}"/>></p>
        <p>邮箱：<input type="text" name="mail" value="${member.mail!}"/></p>
        <input type="submit" value="更新"/>
    </form>
    <form action="/m/member-mine/member-password" method="post">
        <p>旧密码：<input type="password" name="orlpassword" value=""/><#if passwordWrong??><b>密码错误</b></#if></p>
        <p>新密码：<input type="password" name="password"/>
        <@spring.bind "member.password" />
        <@spring.showErrors "<br/>"/></p>
        <p><input type="submit" value="更改"/></p>
    </form>
</div>