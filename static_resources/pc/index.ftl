<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-首页</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>

<body>
<div class="page">
    <#include "header.ftl"/>

        <div id="maincontent">
            <div id="fresh_racking">
                <h1>
                    <span class="bannertitle">最新上架</span>
                </h1>
                <#if itemMap['fresh_racking']??>
                    <#list itemMap['fresh_racking'] as item>
                        <div class="imgitem">
                            <#if item.avatarPath??>
                                <a href="/goodies/item-${item.itemid}"><img width="200" height="300" src="${item.avatarPath}"/></a>
                            </#if>
                            <div class="itemdesc">
                                ${item.itemName}
                                    <br>
                                ${item.price}
                            </div>
                        </div>
                    </#list>
                </#if>
            </div>
            <div id="hot_sale">
                <h1>
                    <span class="bannertitle">热销单品</span>
                </h1>
                <#if itemMap['hot_sale']??>
                    <#list itemMap['hot_sale'] as item>
                        <div class="imgitem">
                            <#if item.avatarPath??>
                                <a href="/goodies/item-${item.itemid}"><img width="200" height="300" src="${item.avatarPath}"/></a>
                            </#if>
                            <div class="itemdesc">
                                ${item.itemName}
                                <br>
                                ${item.price}
                            </div>
                        </div>
                    </#list>
                </#if>
            </div>
            <div id="recommend">
                <h1>
                    <span class="bannertitle">推荐榜单</span>
                </h1>
                <#if itemMap['recommend']??>
                    <#list itemMap['recommend'] as item>
                        <div class="imgitem">
                            <#if item.avatarPath??>
                                <a href="/goodies/item-${item.itemid}"><img width="200" height="300" src="${item.avatarPath}"/></a>
                            </#if>
                            <div class="itemdesc">
                            ${item.itemName}
                                <br>
                            ${item.price}
                            </div>
                        </div>
                    </#list>
                </#if>
            </div>
            <div id="exclusive">
                <h1>
                    <span class="bannertitle">独家发售</span>
                </h1>
                <#if itemMap['exclusive']??>
                    <#list itemMap['exclusive'] as item>
                        <div class="imgitem">
                            <#if item.avatarPath??>
                                <a href="/goodies/item-${item.itemid}"><img width="200" height="300" src="${item.avatarPath}"/></a>
                            </#if>
                            <div class="itemdesc">
                            ${item.itemName}
                                <br>
                            ${item.price}
                            </div>
                        </div>
                    </#list>
                </#if>
            </div>
            <div id="fashionable">
                <h1>
                    <span class="bannertitle">风尚必备</span>
                </h1>
            <#if itemMap['fashionable']??>
                <#list itemMap['fashionable'] as item>
                    <div class="imgitem">
                        <#if item.avatarPath??>
                            <a href="/goodies/item-${item.itemid}"><img width="200" height="300" src="${item.avatarPath}"/></a>
                        </#if>
                        <div class="itemdesc">
                        ${item.itemName}
                            <br>
                        ${item.price}
                        </div>
                    </div>
                </#list>
            </#if>

            </div>
            <#--<div id="last_week">-->
                <#--<h1>-->
                    <#--<span class="bannertitle">&nbsp;上&nbsp;周&nbsp;</span>-->
                <#--</h1>-->
                <#--<#if itemMap['last_week']??>-->
                    <#--<#list itemMap['last_week'] as item>-->
                        <#--<div class="imgitem">-->
                            <#--<#if item.avatarPath??>-->
                                <#--<img width="200" height="300" src="${item.avatarPath}"/>-->
                            <#--</#if>-->
                            <#--<div class="itemdesc">-->
                            <#--${item.itemName}-->
                                <#--<br>-->
                            <#--${item.price}-->
                            <#--</div>-->
                        <#--</div>-->
                    <#--</#list>-->
                <#--</#if>-->
            <#--</div>-->
            <div id="replenishment">
                <h1>
                    <span class="bannertitle">&nbsp;补&nbsp;货&nbsp;</span>
                </h1>
                <#if itemMap['replenishment']??>
                    <#list itemMap['replenishment'] as item>
                        <div class="imgitem">
                            <#if item.avatarPath??>
                                <a href="/goodies/item-${item.itemid}"><img width="200" height="300" src="${item.avatarPath}"/></a>
                            </#if>
                            <div class="itemdesc">
                            ${item.itemName}
                                <br>
                            ${item.price}
                            </div>
                        </div>
                    </#list>
                </#if>
            </div>

        </div>
    <#include "footer.ftl"/>
</div>
</body>
</html>



