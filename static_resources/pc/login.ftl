<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-账户信息</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
<#include "backheader.ftl"/>
    <div id="loginform">
        <form action="/members/member-login" method="post">

            <div>用户：<input name="passportid" value="${passportid!}"/></div>
            <br>

            <div>密码：<input name="password" value="${password!}" type="password"/></div>
            <div><br>
                保存登录状态：<input type="checkbox" name="keepalive" checked="checked"/>

            </div>
            <div><#if loginError??&&loginError==true><b>用户名或者密码错误，请重试</b></#if></div>
            <input type="submit" value="提交">
        </form>
    </div>
<#include "footer.ftl"/>
</div>
</body>
</html>

