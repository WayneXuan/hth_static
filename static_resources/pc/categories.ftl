<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>分类管理</title>
    <link rel="stylesheet" href="/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_categories.css" type="text/css">
    <link rel="stylesheet" href="/css/jquery.alerts.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_common.css" type="text/css">


    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.alert.js"></script>
    <script type="text/javascript" src="/js/jquery.ztree.all-3.5.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/trees.js"></script>
</head>
<body>
<div class="page">
    <#include "backheader.ftl"/>
    <#include "leftnav.ftl"/>
    <div id="TreeFrameUp">
        <div id="crudbutton_c1">
            <button id="treeC1_new">新增</button>
            <button id="treeC1_del">删除</button>
            <button id="treeC1_mod">修改</button>
        </div>
        <div id="Style_treeC1" class="treeborder">
            <ul id="tree_c1" class="ztree"></ul>
        </div>

        <div id="moveBrand">
            <button id="moveBrand_left"><<</button>
            <br>
            <button id="moveBrand_right">>></button>
        </div>

        <div id="crudbutton_b">
            <button id="treeB_new">新增</button>
            <button id="treeB_del">删除</button>
            <button id="treeB_mod">修改</button>
        </div>
        <div id="Style_treeB" class="treeborder">
            <ul id="tree_b" class="ztree"></ul>
        </div>
    </div>
<#--<div id="line"></div>-->
    <div id="TreeFrameDown">
        <div id="Style_treeC2" class="treeborder">
            <ul id="tree_c2" class="ztree"></ul>
        </div>

        <div id="crudbutton_p">
            <button id="treeP_new">新增</button>
            <button id="treeP_del">删除</button>
            <button id="treeP_mod">修改</button>
        </div>
        <div id="Style_treeP" class="treeborder">
            <ul id="tree_p" class="ztree"></ul>
        </div>

        <div id="crudbutton_o">
            <button id="treeO_new">新增</button>
            <button id="treeO_del">删除</button>
            <button id="treeO_mod">修改</button>
        </div>
        <div id="Style_treeO" class="treeborder">
            <ul id="tree_o" class="ztree"></ul>
        </div>
    </div>
    <#include "footer.ftl"/>
</div>



</body>
</html>