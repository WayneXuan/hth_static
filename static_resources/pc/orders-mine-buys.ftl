<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-我的订单</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>
    <#include "member-leftnav.ftl"/>
    <div id="orders_mine_buys_list">
    <table style="width: 600px;">
        <thead align="left">
        <tr>
            <th>订单号</th>
            <th>价格</th>
            <th>下单日期</th>
            <th>订单状态</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <#if pageModel??&&pageModel.pageList??>
            <#list pageModel.pageList as orders>
            <tr>
                <td><a href="/orders-mine/order-${orders.ordersSerialid}">${orders.ordersSerialid!}</a></td>
                <td>${orders.totalPrice!}</td>
                <td>${orders.orderTime?datetime!}</td>
                <td>
                    <#if orders.status??&&orders.status=='Submitted'>已下单<#elseif orders.status??&&orders.status=='Paid'>已付款<#elseif orders.status??&&orders.status=='Discarded'>已作废</#if>
                </td>
                <td>
                    <a href="/orders-mine/order-${orders.ordersSerialid}" target="_blank">查看</a>
                </td>

            </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>

    <div id="pagination">
<#if pageModel??&&pageModel.pageNumber??&&(pageModel.pageNumber>1)>
    <a href="/orders-mine/buys?pageNumber=${(pageModel.pageNumber-1)!}<#list pageModel.filterMap?keys as curKey>
&${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}
<#elseif pageModel.filterMap[curKey]?is_boolean>${pageModel.filterMap[curKey]?c}<#else>${pageModel.filterMap[curKey]}</#if>
</#list>">上一页</a> </#if>
    第${(pageModel.pageNumber)!}/${(pageModel.totalPage)!}页
<#if pageModel??&&pageModel.pageNumber??&&pageModel.totalPage??&&(pageModel.pageNumber<pageModel.totalPage)>
    <a href="/orders-mine/buys?pageNumber=${(pageModel.pageNumber+1)!}<#list pageModel.filterMap?keys as curKey>
&${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}
<#elseif pageModel.filterMap[curKey]?is_boolean>${pageModel.filterMap[curKey]?c}<#else>${pageModel.filterMap[curKey]}</#if>
</#list>">下一页</a> </#if>
</div>
    <#include "footer.ftl"/>
</div>
</body>
</html>