<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-精选频道</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>

    <div id="category_path">
        <#if goodiesFilter??&&goodiesFilter.categoryList??&&goodiesFilter.categoryList?size gt 0>
            <#list goodiesFilter.categoryList as category>
                <a href="/channel-${category.categoryid!}">${category.categoryName!}</a>&gt;
            </#list>
        </#if>
    </div>
    <div id="selection">
        <div id="filters">
        <#if goodiesFilter??>
            <#if goodiesFilter.leafCategoryList??&&goodiesFilter.leafCategoryList?size gt 0>
                <div id="category_leaf">
                    分类：
                    <#list goodiesFilter.leafCategoryList as category>
                        <a href="/goodies?${pageModel.addLinkParam('categoryid',category.categoryid)}">${category.categoryName}</a>
                    </#list>
                </div>
            </#if>
            <div id="brand_list">
                品牌：
                <#if goodiesFilter.brand??>
                    <a class="selected_option" href="/goodies?${pageModel.removeLinkParam('brandid',goodiesFilter.brand.brandid)}">${goodiesFilter.brand.brandName!}</a>
                <#elseif goodiesFilter.brandList??&&goodiesFilter.brandList?size gt 0>
                    <#list goodiesFilter.brandList as brand>
                        <a class="option" href="/goodies?${pageModel.addLinkParam('brandid',brand.brandid)}">${brand.brandName}</a>
                    </#list>
                </#if>
            </div>
            <#if goodiesFilter.categoryPropertyList??&&goodiesFilter.categoryPropertyList?size gt 0>
                <div id="property_list">
                    <#list goodiesFilter.categoryPropertyList as categoryProperty>
                        <div>
                        ${categoryProperty.propertyName!}:
                            <#if goodiesFilter.propertyidOptionMap??&&goodiesFilter.propertyidOptionMap[categoryProperty.propertyid?string]??>
                                <#assign selectedOption=goodiesFilter.propertyidOptionMap[categoryProperty.propertyid?string]>
                                <a class="selected_option" href="/goodies?${pageModel.removeLinkParam('optionids',selectedOption.optionid)}">${selectedOption.optionName!}</a>
                            <#else >
                                <#list categoryProperty.propertyOptionSet as option>
                                    <a class="option" href="/goodies?${pageModel.addLinkParam('optionids',option.optionid)}">${option.optionName!}</a>
                                </#list>
                            </#if>

                        </div>
                    </#list>
                </div>
            </#if>
        </#if>
        </div>
        <div id="display_order">
        <#if pageModel.orderkey??&&pageModel.orderkey=='orders'>
            <#if pageModel.order??&&pageModel.order=='asc'>
                <a href="/goodies?${pageModel.addLinkParam('orderkey','orders')}&order=desc">销量 ↓</a>
            <#else >
                <a href="/goodies?${pageModel.addLinkParam('orderkey','orders')}&order=asc">销量 ↑</a>
            </#if>
        <#else >
            <a href="/goodies?${pageModel.addLinkParam('orderkey','orders')}&order=desc">销量</a>
        </#if>
        <#if pageModel.orderkey??&&pageModel.orderkey=='rack'>
            <#if pageModel.order??&&pageModel.order=='asc'>
                <a href="/goodies?${pageModel.addLinkParam('orderkey','rack')}&order=desc">时间 ↓</a>
            <#else >
                <a href="/goodies?${pageModel.addLinkParam('orderkey','rack')}&order=asc">时间 ↑</a>
            </#if>
        <#else >
            <a href="/goodies?${pageModel.addLinkParam('orderkey','rack')}&order=desc">时间</a>
        </#if>
        <#if pageModel.orderkey??&&pageModel.orderkey=='price'>
            <#if pageModel.order??&&pageModel.order=='asc'>
                <a href="/goodies?${pageModel.addLinkParam('orderkey','price')}&order=desc">价格 ↓</a>
            <#else >
                <a href="/goodies?${pageModel.addLinkParam('orderkey','price')}&order=asc">价格 ↑</a>

            </#if>
        <#else >
            <a href="/goodies?${pageModel.addLinkParam('orderkey','price')}&order=desc">价格</a>
        </#if>
        </div>
        <div id="goodies_list">
        <#if pageModel.pageList??>
            <#list pageModel.pageList as item>
                <div><#if item.avatarPath??>
                    <img src="${item.avatarPath}"/>
                </#if>
                <br>
                ${item.itemName}
                ${item.price}
                </div>
            </#list>
        </#if>

        </div>

        <div id="pagination">
        <#if pageModel??&&pageModel.pageNumber??&&(pageModel.pageNumber>1)>
            <a href="/goodies?pagenumber=${(pageModel.pageNumber-1)!}<#list pageModel.filterMap?keys as curKey>
<#if pageModel.filterMap[curKey]?is_date>&${curKey}=${pageModel.filterMap[curKey]?date}
<#elseif pageModel.filterMap[curKey]?is_boolean>&${curKey}=${pageModel.filterMap[curKey]?c}
<#elseif pageModel.filterMap[curKey]?is_collection&&pageModel.filterMap[curKey]?size gt 0>
<#list pageModel.filterMap[curKey] as curItem>&${curKey}=${curItem}</#list>
<#else>&${curKey}=${pageModel.filterMap[curKey]}</#if>
</#list><#if pageModel.orderkey??&&pageModel.order??>&orderkey=${pageModel.orderkey}&order=${pageModel.order}</#if>">上一页</a> </#if>
            第${(pageModel.pageNumber)!}/${(pageModel.totalPage)!}页
        <#if pageModel??&&pageModel.pageNumber??&&pageModel.totalPage??&&(pageModel.pageNumber<pageModel.totalPage)>
            <a href="/goodies?pagenumber=${(pageModel.pageNumber+1)!}<#list pageModel.filterMap?keys as curKey>
<#if pageModel.filterMap[curKey]?is_date>&${curKey}=${pageModel.filterMap[curKey]?date}
<#elseif pageModel.filterMap[curKey]?is_boolean>&${curKey}=${pageModel.filterMap[curKey]?c}
<#elseif pageModel.filterMap[curKey]?is_collection&&pageModel.filterMap[curKey]?size gt 0>
<#list pageModel.filterMap[curKey] as curItem>&${curKey}=${curItem}</#list>
<#else>&${curKey}=${pageModel.filterMap[curKey]}</#if>
</#list><#if pageModel.orderkey??&&pageModel.order??>&orderkey=${pageModel.orderkey}&order=${pageModel.order}</#if>">下一页</a> </#if>
        </div>
    </div>
    <#include "footer.ftl"/>

</div>
</body>
</html>