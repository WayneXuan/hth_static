<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html;image/jpeg;image/jpg;image/png;charset=UTF-8">
    <title>商品列表</title>
    <link rel="stylesheet" href="/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_categories.css" type="text/css">
    <link rel="stylesheet" href="/css/jquery.alerts.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_common.css" type="text/css">
    <#--<link rel="stylesheet" href="/js/uploadify.css" type="text/css">-->


    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.alert.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/highcourt/js/items.js"></script>

<#--<script type="text/javascript" src="/js/jquery.uploadify.min.js"></script>-->
    <script type="application/javascript">
        $(function() {
            $("select[name='statusSelect']").change(function () {
                location.href="/items/item-status-"+$(this).attr("id").substr(13)+"?status="+$(this).val();
            });
        });
    </script>
</head>
<body>
<div class="page">
    <#include "backheader.ftl"/>
    <#include "leftnav.ftl"/>
    <div class="rightcontent">

    <div id="filters">
        <form action="/items" method="get">

        <div>商品编号或者商品描述：<input type="text" name="keyword" value="${(pageModel.filterMap['keyword'])!}"/> </div>
        <div id="starttime">开始时间：<input type="text" name="beginTime" value="${(pageModel.filterMap['beginTime']?date)!}"/> </div>
        <div id="endtime">结束时间：<input type="text" name="endTime" value="${(pageModel.filterMap['endTime']?date)!}"/> </div>
        <div class="buhuo">补货<input type="checkbox" name="replenish"
                      <#if pageModel.filterMap['replenish']??&&pageModel.filterMap['replenish']==true>checked="checked"</#if>/>
        </div>
        <div class="buhuo">推荐<input type="checkbox" name="recommend"
                      <#if pageModel.filterMap['recommend']??&&pageModel.filterMap['recommend']==true>checked="checked"</#if>/>
        </div>
        <div class="buhuo">独家<input type="checkbox" name="exclusive"
                      <#if pageModel.filterMap['exclusive']??&&pageModel.filterMap['exclusive']==true>checked="checked"</#if>/>
        </div>
        <div>风尚<input type="checkbox" name="fashionable"
                      <#if pageModel.filterMap['fashionable']??&&pageModel.filterMap['fashionable']==true>checked="checked"</#if>/>
        </div>

        <div>
            <div id="cates">分类<select name="categoryids"></select></div>
            <input type="hidden" name="categoryid" value="${(pageModel.filterMap['categoryid'])!}"/>
        </div>

        <div class="buhuo">
            <div id="brands">品牌<select name="brandid"></select></div>
            <input type="hidden" name="brandid" value="${(pageModel.filterMap['brandid'])!}"/>
        </div>
        <div><input type="submit" value="查询"/></div>
        </form>
    </div>

    <div>
    <br>
    <table border="1" width="100%" cellspacing="0" cellpadding="0" id="items_table">
        <thead>
        <tr>
            <th>图片</th>
            <th>商品信息</th>
            <th>品牌</th>
            <th>上架日期</th>
            <th>商品价格</th>
            <th>类别</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <#if pageModel??&&pageModel.pageList??>
            <#list pageModel.pageList as item>
            <tr><td><#if item.avatarPath??><img width="60" height="90" src="/img/${item.avatarPath}_p.jpg"/></#if></td>
                <td>
                <div>编号：${item.itemid!}</div>
                <div>${item.itemName!}</div>
                    <#list item.propertyOptionList as option>
                        <div>${(option.categoryProperty.propertyName)!}:${option.optionName!}</div>
                    </#list>
                </td>
                <td>${item.brand.brandName!}</td><td>${item.rackingTime?datetime!}</td><td>${item.price!}</td>
                <td><#if item.categoryList??><#list item.categoryList as category>${category.categoryName}&nbsp;</#list></#if></td>
                <td><a href="/items/item-edit-${item.itemid}">编辑</a>
                    <select id="statusSelect-${item.itemid}" name="statusSelect">
                        <option value="OnSale" <#if item.status=='OnSale'>selected="selected" </#if>>上架</option>
                        <option value="Replenishment" <#if item.status=='Replenishment'>selected="selected" </#if>>补货</option>
                        <option value="OffSelf" <#if item.status=='OffSelf'>selected="selected" </#if>>下架</option>
                    </select>
                    <a href="/items/item-status-${item.itemid}?isdeleted=1">删除</a>
                </td>
            </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>
    <div id="pagination">
<#if pageModel??&&pageModel.pageNumber??&&(pageModel.pageNumber>1)>
    <a href="/items?pagenumber=${(pageModel.pageNumber-1)!}<#list pageModel.filterMap?keys as curKey>
&${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}
<#elseif pageModel.filterMap[curKey]?is_boolean>${pageModel.filterMap[curKey]?c}<#else>${pageModel.filterMap[curKey]}</#if>
</#list>">上一页</a> </#if>
    第${(pageModel.pageNumber)!}/${(pageModel.totalPage)!}页
<#if pageModel??&&pageModel.pageNumber??&&pageModel.totalPage??&&(pageModel.pageNumber<pageModel.totalPage)>
    <a href="/items?pagenumber=${(pageModel.pageNumber+1)!}<#list pageModel.filterMap?keys as curKey>
&${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}
<#elseif pageModel.filterMap[curKey]?is_boolean>${pageModel.filterMap[curKey]?c}<#else>${pageModel.filterMap[curKey]}</#if>
</#list>">下一页</a> </#if>
</div>
</div>
    <#include "footer.ftl"/>
</div>
</body>
</html>