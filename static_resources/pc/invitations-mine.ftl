<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-首页</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
    <script type="application/javascript">
        var registerUrl = "http://" + window.location.host + "/members/member-register";
        $(function () {
            if($("#invitation_code").val()!="") {
                $("#invitation_code").val(registerUrl+"?invitationcode="+$("#invitation_code").val());
            }

            $("input[name='invitation_select']").click(function() {
                $("#invitation_code").val(registerUrl+"?invitationcode="+$(this).val());
            });

            $("a[name^='discard_']").each(function() {
                $(this).click(function(){
                    var invitationid = $(this).attr("name").substring(8);var data_send;

                    $.ajax({
                        url:"/invitations-mine/invitation-discard-"+invitationid,
                        type:"post",
                        data:null,
                        dataType: "json",
                        success:function(data) {
                            if(data.status==0) {
                                $("a[name='discard_" + invitationid + "']").remove();
                            }
                        }
                    });

                });
            });
        });
    </script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>

    <div id="filter_form">
        查询条件：
        <form action="/invitations-mine" method="get">
            开始时间生效：<input type="text" name="beginTime" value="${(pageModel.filterMap['beginTime']?date)!}">
            结束时间生效：<input type="text" name="endTime" value="${(pageModel.filterMap['endTime']?date)!}">
            <input type="submit" value="查询"/>
        </form>
    </div>
    <div id="generate_code">
        <form action="/invitations-mine/invitation-new">
            折扣率：<input type="text" name="discount" value="">
            <input type="submit" value="生成邀请码"/>
        </form>
        <input id="invitation_code" value="${latestInvitationCode!}" size="100">
    </div>
    <div id="invitation_table">
        <label>邀请记录</label>
        <table>
            <thead>
            <tr>
                <th></th><th>邀请码</th><th>折扣率</th><th>开始时间</th><th>结束时间</th><th>操作</th>
            </tr>
            </thead>
            <tbody>
        <#if pageModel??&&pageModel.pageList??>
        <#list pageModel.pageList as invitation>
            <tr>
                <td><input type="radio" name="invitation_select" value="${invitation.invitationCode!}"></td>
                <td>${invitation.invitationCode!}</td>
                <td>${invitation.discount!}</td>
                <td>${(invitation.beginTime?datetime)!}</td>
                <td>${(invitation.endTime?datetime)!}</td>
                <td><#if invitation.status!='Discarded'><a name="discard_${invitation.invitationid!}" href="javascript:void(0)">作废</a> </#if></td>
            </tr>
        </#list>
        </#if>

            </tbody>
        </table>
    </div>
    <div id="pagination">
    <#if pageModel??&&pageModel.pageNumber??&&(pageModel.pageNumber>1)>
        <a href="/invitations-mine?pageNumber=${(pageModel.pageNumber-1)!}<#list pageModel.filterMap?keys as curKey>
    &${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}<#else>${pageModel.filterMap[curKey]}</#if>
    </#list>">上一页</a> </#if>
        第${(pageModel.pageNumber)!}/${(pageModel.totalPage)!}页
    <#if pageModel??&&pageModel.pageNumber??&&pageModel.totalPage??&&(pageModel.pageNumber<pageModel.totalPage)>
        <a href="/invitations-mine?pageNumber=${(pageModel.pageNumber+1)!}<#list pageModel.filterMap?keys as curKey>
    &${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}<#else>${pageModel.filterMap[curKey]}</#if>
    </#list>">下一页</a> </#if>
    </div>
</div>
</body>
</html>