<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html;image/jpeg;image/jpg;image/png;charset=UTF-8">
    <title>管理客户</title>
    <link rel="stylesheet" href="/css/pcStyle_backend_categories.css" type="text/css">
    <link rel="stylesheet" href="/css/jquery.alerts.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_common.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.alert.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
<#--<script type="text/javascript" src="/highcourt/js/item-edit.js"></script>-->
    <script type="application/javascript">


        $(function () {
            $("a[name='orders_discard']").each(function () {
                var ordersid = $(this).attr("id").substr(15);
                $(this).click(function () {
                    if ($("#orders_discard_" + ordersid).html() == "作废") {
                        $.ajax({
                            url: "/orders/order-discard-" + ordersid,
                            type: "post",
                            data: null,
                            dataType: "json",
                            success: function (data) {
                                if (data.status == 0) {
                                    $("#orders_discard_" + ordersid).html("已作废");
                                }
                            }
                        });
                    }
                });
            });
        });
    </script>
</head>
<body>
<div class="page">
<#include "backheader.ftl"/>
<#include "leftnav.ftl"/>
    <div class="rightcontent">
        <div id="filterForm">
            <form action="/orders" method="get">
                <label>关键字</label><input name="keyword" value="${(pageModel.filterMap['keyword'])!}"/>
                <label>开始时间</label><input name="beginTime" value="${(pageModel.filterMap['beginTime']?date)!}"/>
                <label>结束时间</label><input name="endTime" value="${(pageModel.filterMap['endTime']?date)!}"/>
                <br><label>状态</label>
                <select name="status">
                    <option value="">全部状态</option>
                    <option value="Submitted"
                            <#if pageModel.filterMap['status']??&&pageModel.filterMap['status']=="Submitted">selected="selected" </#if>>
                        已下单
                    </option>
                    <option value="Paid"
                            <#if pageModel.filterMap['status']??&&pageModel.filterMap['status']=="Paid">selected="selected" </#if>>
                        已付款
                    </option>
                    <option value="Discarded"
                            <#if pageModel.filterMap['status']??&&pageModel.filterMap['status']=="Discarded">selected="selected" </#if>>
                        已作废
                    </option>
                </select>
                <input type="submit" value="查询">
            </form>
        </div>

        <div id="list" style="margin-top: 20px;">
            <table style="width: 600px;">
                <thead align="left">
                <tr>
                    <th>订单号</th>
                    <th>日期</th>
                    <th>用户名</th>
                    <th>商品编号名称</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <#if pageModel??&&pageModel.pageList??>
                    <#list pageModel.pageList as orders>
                    <tr>
                        <td>${orders.ordersSerialid!}</td>
                        <td>${orders.orderTime?datetime!}</td>
                        <td>${(orders.buyerMember.passportid)!}</td>
                        <td><#if orders.ordersItemMapSet??>
                            <#list orders.ordersItemMapSet as map>
                            ${(map.item.itemid)!}:${(map.item.itemName)!}
                            </#list>
                            <div></div>
                        </#if>
                        </td>
                        <td>
                            <#if orders.status??&&orders.status=='Submitted'>
                                已下单<#elseif orders.status??&&orders.status=='Paid'>
                                已付款<#elseif orders.status??&&orders.status=='Discarded'>已作废</#if>
                        </td>
                        <td>
                            <a href="/orders/order-${orders.ordersid}" target="_blank">查看</a>
                            <a id="orders_discard_${orders.ordersid}" name="orders_discard"
                               href="javascript:void(0)"><#if orders.status??&&orders.status=='Discarded'>已作废<#else >
                                作废</#if></a>
                        </td>

                    </tr>
                    </#list>
                </#if>
                </tbody>
            </table>
        </div>

        <div id="pagination">
        <#if pageModel??&&pageModel.pageNumber??&&(pageModel.pageNumber>1)>
            <a href="/items?pagenumber=${(pageModel.pageNumber-1)!}<#list pageModel.filterMap?keys as curKey>
        &${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}
        <#elseif pageModel.filterMap[curKey]?is_boolean>${pageModel.filterMap[curKey]?c}<#else>${pageModel.filterMap[curKey]}</#if>
        </#list>">上一页</a> </#if>
            第${(pageModel.pageNumber)!}/${(pageModel.totalPage)!}页
        <#if pageModel??&&pageModel.pageNumber??&&pageModel.totalPage??&&(pageModel.pageNumber<pageModel.totalPage)>
            <a href="/items?pagenumber=${(pageModel.pageNumber+1)!}<#list pageModel.filterMap?keys as curKey>
        &${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}
        <#elseif pageModel.filterMap[curKey]?is_boolean>${pageModel.filterMap[curKey]?c}<#else>${pageModel.filterMap[curKey]}</#if>
        </#list>">下一页</a> </#if>
        </div>
    </div>
<#include "footer.ftl"/>
</div>

</body>

</html>