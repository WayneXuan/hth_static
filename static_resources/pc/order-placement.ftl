<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-首页</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>
    <div>现有地址</div>
    <div>
        <p>您的购物车</p>
        <table>
            <thead>
            <tr>
                <th>商品</th><th>商品价格</th><th>数量</th><th>总价</th>
            </tr>
            </thead>
            <tbody>
            <#assign totalPrice=0>
            <#list cartList as cart>
            <tr id="row_${cart_index}">
                <td>
                    <input id="item_${cart.itemid}" type="hidden" name="carts[${cart_index}].itemid" value="${cart.itemid}"/>

                    <#if cart.itemShow.avatarPath??>
                        <img src="${cart.itemShow.avatarPath}"/>
                    </#if>
                    <label>${cart.itemShow.itemName!}</label>
                    <#if cart.itemShow.propertyOptionList??>
                        <#list cart.itemShow.propertyOptionList as option>
                        ${(option.categoryProperty.propertyName)!}：${(option.optionName)!}
                        </#list>
                    </#if>

                </td>
                <td>
                ${cart.itemShow.price!}
                </td>
                <td>
                ${cart.count}
                </td>
                <td>
                ${cart.itemShow.price?double*cart.count?int}
                <#assign totalPrice=(totalPrice+cart.itemShow.price?double*cart.count?int)>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
    <div>
        <p>订单结算</p>
        <div>
            <p>订单总计：${totalPrice!}</p>
            <form action="/orders-mine/order-placement" method="post">
                <input type="hidden" name="timestamp" value="${(cartList[0].operateTime?string('yyyyMMddHHmmsssss'))!}">
                <input type="hidden" name="consigneeid" value="1">
                <p>留言：<textarea name="message"></textarea></p>
                <p><input type="submit" value="提交"/></p>
            </form>
        </div>
    </div>
    <#include "footer.ftl"/>
</div>
</body>
</html>