<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-首页</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
<#include "header.ftl"/>

<div id="filter_form">
    <form action="/invitations-mine/invitation-register" method="get">
        开始时间注册：<input type="text" name="beginTime" value="${(pageModel.filterMap['beginTime']?date)!}">
        结束时间注册：<input type="text" name="endTime" value="${(pageModel.filterMap['endTime']?date)!}">
        <input type="submit" value="查询"/>
    </form>
</div>
<div id="member_table">
    <label>已注册会员</label>
    <table>
        <thead>
        <tr>
            <th>用户名</th><th>邀请码</th><th>邀请生成时间</th><th>注册时间</th><th>折扣率</th>
        </tr>
        </thead>
        <tbody>
        <#if pageModel??&&pageModel.pageList??>
            <#list pageModel.pageList as authorization>
                <tr>
                    <td>${(authorization.authrisedMember.passportid)!}</td>
                    <td>${(authorization.invitation.invitationCode)!}</td>
                    <td>${(authorization.invitation.createTime?datetime)!}</td>
                    <td>${(authorization.authoriseTime?datetime)!}</td>
                    <td>${(authorization.discount)!}</td>
                </tr>
            </#list>
        </#if>
        </tbody>
    </table>
</div>
<div id="pagination">
<#if pageModel??&&pageModel.pageNumber??&&(pageModel.pageNumber>1)>
    <a href="/invitations-mine/invitation-register?pageNumber=${(pageModel.pageNumber-1)!}<#list pageModel.filterMap?keys as curKey>
&${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}<#else>${pageModel.filterMap[curKey]}</#if>
</#list>">上一页</a> </#if>
    第${(pageModel.pageNumber)!}/${(pageModel.totalPage)!}页
<#if pageModel??&&pageModel.pageNumber??&&pageModel.totalPage??&&(pageModel.pageNumber<pageModel.totalPage)>
    <a href="/invitations-mine/invitation-register?pageNumber=${(pageModel.pageNumber+1)!}<#list pageModel.filterMap?keys as curKey>
&${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}<#else>${pageModel.filterMap[curKey]}</#if>
</#list>">下一页</a> </#if>
</div>
</div>
</body>
</html>