<div id="left-nav">
    <div id="nav_top">管理员</div>
    <div class="nav_line"></div>
    <div class="nav_menu">管理交易</div>
    <div class="nav_submenu"><a href="/orders">管理订单</a></div>
    <div class="nav_line"></div>
    <div class="nav_menu">管理商品</div>
    <div class="nav_submenu"><a href="/items/item-new">新增商品</a></div>
    <div class="nav_submenu"><a href="/items">编辑商品</a></div>
    <div class="nav_line"></div>
    <div class="nav_menu">管理客户</div>
    <div class="nav_submenu"><a href="/members">管理客户</a></div>
    <div class="nav_line"></div>
    <div class="nav_menu">分类和品牌</div>
    <div class="nav_submenu"><a href="/categories">分类和品牌</a></div>
</div>