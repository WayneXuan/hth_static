<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-我的订单</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>
    <div id="orders_mine_ordermgt">
        <div id="path">
            <a href="/">首页</a>&gt;<a href="/orders-mine/buys">订单管理</a>&gt;${(orders.ordersSerialid)!}
        </div>
        <div id="status">
            订单：${(orders.ordersSerialid)!}&nbsp;&nbsp;订单状态：
        <#if orders.status??&&orders.status=='Submitted'>已提交<#elseif orders.status??&&orders.status=='Paid'>已付款<#elseif orders.status??&&orders.status=='Discarded'>已作废</#if>
        </div>
        <div id="orders_detail">
            <p>订单详细信息</p>
        <#if orders.ordersConsignee??>
            <div>
                <p>送货地址</p>
                <p>${orders.ordersConsignee.consigneeName!}，${orders.ordersConsignee.consigneeAddress!}
                    ，${orders.ordersConsignee.postcode!}，
                ${(addressProvinceMap[orders.ordersConsignee.provinceid?string].provinceName)!}
                    &nbsp;${(addressCityMap[orders.ordersConsignee.cityid?string].cityName)!}
                    &nbsp;${(addressAreaMap[orders.ordersConsignee.areaid?string].areaName)!}
                    ，${orders.ordersConsignee.consigneePhone!}
                </p>
            </div>
        </#if>

            <div>
                <p>商品详细信息</p>
                <table>
                    <#--<thead>-->
                    <#--<tr><th>商品图片</th><th>商品信息</th><th>商品价格</th><th>数量</th><th>总计</th></tr>-->
                    <#--</thead>-->
                    <tbody>
                    <#if orders.ordersItemMapSet??>
                        <#list orders.ordersItemMapSet as ordersItemMap>
                            <#if ordersItemMap.item??>
                            <tr>
                                <td>
                                    <#if ordersItemMap.item.avatarPath??>
                                        <img src="/img/${ordersItemMap.item.avatarPath}_p.jpg"/>
                                    </#if>

                                    <#if ordersItemMap.item.propertyOptionList??>
                                        <#list ordersItemMap.item.propertyOptionList as option>
                                        ${(option.categoryProperty.propertyName)!}：${(option.optionName)!}
                                        </#list>
                                    </#if>
                                </td>
                                <td>
                                    商品名称：${ordersItemMap.item.itemName!}
                                </td>
                                <td>
                                价格：${ordersItemMap.discountPrice!},${ordersItemMap.discount!}%折
                                </td>
                                <td>
                                数量：${ordersItemMap.count!}
                                </td>
                                <td>
                                小计：${(ordersItemMap.discountPrice?double*ordersItemMap.count?int)!}
                                </td>

                            </tr>
                            </#if>
                        </#list>
                    </#if>
                    </tbody>
                </table>
            </div>
            <div>
                <div>留言：${orders.message!}</div>
                <div>总计：${orders.totalPrice!}</div>
            </div>
        </div>
    </div>

    <#include "footer.ftl"/>
</div>
</body>
</html>