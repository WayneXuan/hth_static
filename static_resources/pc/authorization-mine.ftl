<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-首页</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
<#include "header.ftl"/>
<div id="authorization">
    <p>授权时间：${(authorization.authoriseTime?date)!}</p>
    <p>授权人信息：${(authorization.authorisorMember.name)!}<br/>${(authorization.authorisorMember.mobile)!}</p>
    <p>折扣率：${(authorization.discount)!}</p>
</div>
</div>
</body>
</html>