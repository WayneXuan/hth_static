<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-购物车</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/cart.js"></script>

    <#--todo 1删除按钮和2+-按钮的json，商品行数和金额的计算，3使用编辑框改数量后金额不会被重新计算，除非在失去焦点时使用ajax更新后，更新面板-->
    <script type="application/javascript">
        $(function() {
            $("a[name^='remove_']").each(function () {
                var index = $(this).attr("name").substr(7);
                $(this).click(function () {
                    $("#status_" + index).val("del");
                    $("#item_" + index).hide();
                    $("#cartitems").css("height",$("#cartitems").height());
                });
            });
        });
    </script>

</head>
<body>
<div class="page">
    <#include "header.ftl"/>

    <div id="cartitems">
        <form action="/cart/item-all" method="post">

        <#if cartList??>
            <#list cartList as cart>
                  <div class="cart_item_block" id="item_${cart_index}">
                      <input id="item_${cart.itemid}" type="hidden" name="carts[${cart_index}].itemid" value="${cart.itemid}"/>
                      <#if cart.itemShow.avatarPath??>
                          <img width="120" height="180" src="${cart.itemShow.avatarPath}"/>
                      </#if>
                      <div class="cart_item_desc">
                          <label>${cart.itemShow.itemName!}</label>
                          <div class="cart_item_po">
                              <#if cart.itemShow.propertyOptionList??>
                                  <#list cart.itemShow.propertyOptionList as option>
                                  ${(option.categoryProperty.propertyName)!}：${(option.optionName)!}
                                  </#list>
                              </#if>
                          </div>

                          <div class="cart_price">
                              ￥${cart.itemShow.price!}
                          </div>
                          <div class="cart_count">
                              X<input class="cart_count_input" name="carts[${cart_index}].count" value="${cart.count}"/>
                          </div>
                          <div class="cart_subtotal">
                              ￥${cart.itemShow.price?double*cart.count?int}
                          </div>
                          <input type="hidden" id="status_${cart_index}" name="carts[${cart_index}].status" value="update"/>
                          <div class="cart_remove">
                              <a name="remove_${cart_index}" href="javascript:void(0)">移除</a>
                          </div>
                      </div>
                  </div>
            </#list>
        </#if>
    </div>
        <div id="cart_removeall">
            <a href="/cart/item-remove-all">移除所有商品</a>
        </div>
        <div id="gosubmit">
            <#--<form action="/" method="post">-->
                <a href="/"><input type="button" value="继续购物"/></a>
            <#--</form>-->
            <input type="hidden" name="order" value="true">
            <input type="submit" value="提交"/>
        </div>
        </form>
    <#include "footer.ftl"/>
</div>
</body>
</html>