<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-商品详情-${item.itemName}</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/item.js"></script>
    <script type="application/javascript">
        $(function() {
            $("form").submit(function () {
                $("#add_count").val($("#count_input").val());
                $("#order_count").val($("#count_input").val());
                return true;
            });
        });
    </script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>

    <div id="category_path">
        <a href="/"> 首页</a>>
        <#if item.categoryList??>
            <#list item.categoryList as category>
                <a href="/channel-${category.categoryid!}">${category.categoryName!}</a>&gt;
            </#list>
        </#if>
    </div>
    <div>
        <div id="item_carousels">
            <#if item.carouselsPictureList??>
            <#list item.carouselsPictureList as carouselsPicture>
                <#if carouselsPicture.filePath??>
                    <img src="/img${carouselsPicture.filePath!}_p.jpg"/>
                </#if>
            </#list>
            </#if>
        </div>
        <div id="item_carousels_thumbnail">
        <#if item.carouselsPictureList??>
            <#list item.carouselsPictureList as carouselsPicture>
                <#if carouselsPicture.filePath??>
                    <img src="/img${carouselsPicture.filePath!}_p.jpg"/>
                </#if>
            </#list>
        </#if>
        </div>
        <div id="item_content">
            <div>${item.itemName}</div>
            <div>原价：${item.price!}，折扣价：${item.discountPrice!}</div>
        </div>
    </div>
    <div id="item_properties">
    <#if item.propertyOptionList??>
        <#list item.propertyOptionList as option>
        <label>${(option.categoryProperty.propertyName)!}:${option.optionName}</label>
    </#list>
    </#if>
    </div>
    <div id="item_description">
        <label>产品描述：</label>
        <div id="item_desc_text">${item.description!}</div>
    </div>
    <div id="item_buy">
        <#if item.status??>
            <#if item.status=='OnSale'>
                &nbsp;数量：<input type="text" id="count_input" value="1">
                <form action="/cart/item-all" method="post">
                    <input type="hidden" name="carts[0].itemid" value="${item.itemid}"/>
                    <input id="add_count" type="hidden" name="carts[0].count" value="1"/>
                    <input type="hidden" name="carts[0].status" value="add"/>
                    <input type="submit" value="加入购物车">
                </form>
                <form action="/cart/item-all" method="post">
                    <input type="hidden" name="carts[0].itemid" value="${item.itemid}"/>
                    <input id="order_count" type="hidden" name="carts[0].count" value="1"/>
                    <input type="hidden" name="carts[0].status" value="add"/>
                    <input type="hidden" name="order" value="true"/>
                    <input type="submit" value="立即购买">
                </form>
            </#if>
            <#if item.status=='Replenishment'>
            <div>缺货</div>
            </#if>
        </#if>
    </div>
    <#include "footer.ftl"/>
</div>
</body>
</html>