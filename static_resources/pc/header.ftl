<div class="header_frame">
    <div class="logo">
        <a href="/"><img src="/css/img/logo-150.png" alt="海天汇 HTH" title="海天汇 HTH"/></a>
    </div>
    <div id="search">
        <form name="search_form" action="/merchandises" method="get">
            <#--<label class="skip" for="search_box">搜索</label>-->
            <input class="search_box" type="search" alt="搜索" placeholder="搜索：请输入商品描述" value="${(pageModel.filterMap['keyword'])!}">
            <#--<input type="submit" value="提交">-->
        </form>
    </div>
    <div id="cart">
        <a href="/orders-mine/buys">我的账户</a>
        <a href="/orders">后台管理</a>
        <a href="/cart/items">购物车￥${(cartListBrief.totalPrice)!'0'}(${(cartListBrief.totalCount)!'0'})</a>
    </div>
    <div id="navigation_bar">
        <ul id="nav-menu" class="sm sm-simple">
            <li><a href="/">首页</a></li>
        <#list categoryList as category>
            <li>
                <#if category.childrenCategory?size gt 0>
                    <a href="/channel-${category.categoryid}">${category.categoryName!}</a>
                    <ul>
                        <#list category.childrenCategory as childCategory>
                            <li>
                                <a href="/goodies?categoryid=${childCategory.categoryid}">${childCategory.categoryName!}</a>
                            </li>
                        </#list>
                    </ul>
                <#else >
                    <a href="/goodies?categoryid=${category.categoryid}">${category.categoryName!}</a>
                </#if>
            </li>
        </#list>
        </ul>

    </div>
</div>
