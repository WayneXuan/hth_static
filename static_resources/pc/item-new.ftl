<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html;image/jpeg;image/jpg;image/png;charset=UTF-8">
    <title>新增商品</title>
    <link rel="stylesheet" href="/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_categories.css" type="text/css">
    <link rel="stylesheet" href="/css/jquery.alerts.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_common.css" type="text/css">
    <link rel="stylesheet" href="/js/uploadify.css" type="text/css">


    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.alert.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/jquery.uploadify.min.js"></script>
    <script type="text/javascript" src="/highcourt/js/item-new.js"></script>
</head>
<body>
<div class="page">
    <#include "backheader.ftl"/>
    <#include "leftnav.ftl"/>
    <div class="rightcontent">

            上传封面图片：<br><input type="file" name="avatarpicture" id="avatarpicture"/>
        <div id="avatar_list"></div>
        <#--<div class="sep_line"></div>-->
        <br>
        <br>上传跑马灯图片：<br><input type="file" name="carouselspicture" id="carouselspicture"/>
        <div id="carousels_list"></div>
        <#--<div class="sep_line"></div>-->

        <form id="item_new_form" action="/items/item-new" method="post" onsubmit="return validate()">
            <div id="hidden_avatars">
                <input type="hidden" name="avatarPictureids" id="avatarPictureids"/>
                <#--<input type="hidden" name="avatarPictureids"/>-->
            </div>
            <div id="hidden_carousels"></div>
            <br><br><br>
            <div id="tops">
                    推荐<input type="checkbox" name="recommend"/>
                    独家<input type="checkbox" name="exclusive"/>
                    时尚<input type="checkbox" name="fashionable"/>
            </div>
            <div class="sep_line"></div>

            <div id="cates">分类<select name="categoryids"></select></div>
            <div id="subcates"><select name="categoryids"></select></div>
            <#--<div id="props">属性<select name="propids"></select></div>-->
            <#--<div id="options">选项<select name="optionids"></select></div>-->
            <div id="brands">品牌<select name="brandid"></select></div>
            <div id="propopts">
                <div class="popairs"></div>
            </div>
            <div id="price">价格<input type="text" name="price"></div>
            <div id="itemname">物品名称<input type="text" name="itemName"/></div>
            <div id="itemdesc">物品描述<textarea name="description"></textarea></div>
            <div><input type="submit" value="提交"/></div>

        </form>
    </div>

</div>
</body>
</html>