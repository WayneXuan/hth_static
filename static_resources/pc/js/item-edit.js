/**
 * Created by pcliang on 2014/6/25.
 */
//定义列表操作类
function  List(listId) {
    this.listId = listId;   //公共变量
}

List.categories=null;            // 静态变量：
List.selectedCate=null;
List.selectedCate_raw=null;
List.selectedSubcate=null;
List.selectedSubcate_raw=null;
List.selectedProp=null;
List.selectedProp_raw=null;
List.selectedOption=null;
List.selectedOption_raw=null;
List.selectedBrand=null;

List.prototype.getlistId = function(){
    return  this.listId;
};
$.ajax({
    url: '/categories/category-tree',
    type: "GET",
    success: function(data) {
        //*********************************************************************//
        //获取树数据
        //*********************************************************************//
        if(data.status==0){
            //存储categories Json数据到List.categories
            List.categories = data.content;
//            初始化分类属性和品牌数据
            $("select option").remove();
            $.each(List.categories,function(k,v){
                $("#cates select").append("<option value='"+ v.categoryid+"'>"+ v.categoryName+"</option>");
                if(k==0){
                    List.selectedCate_raw = v;
                }
            });
            List.selectedCate = $('#cates select').val();

            $.each(List.selectedCate_raw.childrenCategory,function(k,v){
                $("#subcates select").append("<option value='"+ v.categoryid+"'>"+ v.categoryName+"</option>");
                if(k==0){
                    List.selectedSubcate_raw = v;
                }
            });
            List.selectedSubcate = $('#subcates select').val();

            $.each(List.selectedSubcate_raw.categoryPropertySet,function(k,v){
                $("#props select").append("<option value='"+ v.propertyid+"'>"+ v.propertyName+"</option>");
                if(k==0){
                    List.selectedProp_raw = v;
                }
            });
            List.selectedProp = $('#props select').val();

            $.each(List.selectedProp_raw.propertyOptionSet,function(k,v){
                $("#options select").append("<option value='"+ v.optionid+"'>"+ v.optionName+"</option>");
                if(k==0){
                    List.selectedOption = v;
                }
            });
            List.selectedOption = $('#options select').val();

            $.each(List.categories[0].childrenCategory[0].brandCategoryMapSet,function(k,v){
                $("#brands select").append("<option value='"+ v.brandid+"'>"+ v.brand.brandName+"</option>");
            });
            List.selectedBrand = $('#brands select').val();
        }
    }
});

//获取分类
$(document).ready(function() {
    //获取封面和跑马灯图片
    var picurl = $("#picurl").val();
    console.log("hello");
    $.ajax({
        url: picurl,
        type: "GET",
        success: function(data) {
            //*********************************************************************//
//            console.log(data.content[0]);
            $("#avatar_list").append("<img width='40' height='60' src='"+data.content[0]+"'/>");
        }
    });

    //uploadify
    $("#avatarpicture").uploadify({
        swf:'/js/uploadify.swf',
        uploader:'/items/picture-avatar-new',
        cancelImg: '/js/uploadify-cancel.png',
        fileObjName:'avatarpicture',
        fileTypeExts:'*.jpg;*.jpeg;',
        fileSizeLimit:'10MB',
        multi:false,
        formData: {
            picturetype: 'avatar'
        },
            //上传到服务器，服务器返回相应信息到data里
        onUploadSuccess:function(file, data, response){
            var obj = $.parseJSON(data);
            if(obj.status==0){
                $("#avatar_list").append("<img width='40' height='60' src='"+"/img"+obj.content.filePath+"_p.jpg'/>");
            } else{
                jAlert("上传失败，请重试。");
            }

        }
    });
    $("#carouselspicture").uploadify({
        swf:'/js/uploadify.swf',
        uploader:'/items/picture-carousels-new',
        cancelImg: '/js/uploadify-cancel.png',
        fileObjName:'avatarpicture',
        fileTypeExts:'*.jpg;*.jpeg;',
        fileSizeLimit:'10MB',
        multi:false,
        formData: {
            picturetype: 'carousels'
        },
        //上传到服务器，服务器返回相应信息到data里
        onUploadSuccess:function(file, data, response){
            console.log(data);
            var obj = $.parseJSON(data);
            if(obj.status==0){
                $("#carousels_list").append("<img width='40' height='60' src='/img"+obj.content.filePath+"_p.jpg' id='"+obj.content.pictureid+ "'/>");
                //点击删除跑马灯图片
                $("#carousels_list img").bind("click",function(){
                    //获取pictureid
                    var pictureid = this.id;
                    $.ajax({
                        url: '/items/picture-remove-'+this.id,
                        type: "POST",
                        success: function (data) {
//                              console.log('deleted!');
                            //remove element
                            $("#"+pictureid).remove();
                        }
                    });
                });

            } else{
                jAlert("上传失败，请重试。");
            }
        }
    });



    $('#cates select').change(function () {
//       更新子分类和品牌，清空属性列表
        $("#subcates select option").remove();
        $("#props select option").remove();
        $("#options select option").remove();
        $("#brands select option").remove();

        //获取选中categoryid
        List.selectedCate = $('#cates select').val();

        //查找该categoryid下的所有子分类和品牌并添加
        $.each(List.categories,function(k,v){
           if (v.categoryid==List.selectedCate){
               List.selectedCate_raw = v;
               //若该分类下没有子分类则添加该分类的属性
               if(v.childrenCategory.length==0){
                   $.each(v.categoryPropertySet, function (kp,vp) {
                       $("#props select").append("<option value='"+ vp.propertyid+"'>"+ vp.propertyName+"</option>");
                   });
               }
               $.each(v.childrenCategory,function(ks,vs){
                   $("#subcates select").append("<option value='"+ vs.categoryid+"'>"+ vs.categoryName+"</option>");
               });
               //获取默认子分类id，添加属性列表值
               List.selectedSubcate = $('#subcates select').val();
               $.each(v.childrenCategory,function(key,value){
                    if(value.categoryid== List.selectedSubcate) {
                        $.each(value.categoryPropertySet,function(kp,vp){
                            $("#props select").append("<option value='"+ vp.propertyid+"'>"+ vp.propertyName+"</option>");
                        });
                    }
               });

               //获取默认属性，添加选项列表值
               List.selectedProp = $('#props select').val();
               if(v.childrenCategory.length==0){
                   $.each(v.categoryPropertySet,function(kp,vp){
                       if(vp.propertyid==List.selectedProp) {
                           $.each(vp.propertyOptionSet,function(ko,vo){
                               $("#options select").append("<option value='"+ vo.optionid+"'>"+ vo.optionName+"</option>");
                           });
                       }
                   });
               }else{
                   $.each(v.childrenCategory,function(key,value){
                       if(value.categoryid== List.selectedSubcate) {
                           $.each(value.categoryPropertySet,function(kp,vp){
                               if(vp.propertyid==List.selectedProp) {
                                   $.each(vp.propertyOptionSet,function(ko,vo){
                                       $("#options select").append("<option value='"+ vo.optionid+"'>"+ vo.optionName+"</option>");
                                   });
                               }
                           });
                       }
                   });
               }

               List.selectedOption = $('#options select').val();

               $.each(v.brandCategoryMapSet,function(ks,vs){
                   $("#brands select").append("<option value='"+ vs.brandid+"'>"+ vs.brand.brandName+"</option>");
               });
               List.selectedBrand = $('#brands select').val();
           }
        });

    });
    $('#subcates select').change(function () {
//       更新属性列表和品牌列表
        $("#props select option").remove();
        $("#brands select option").remove();
        $("#options select option").remove();

        //获取选中categoryid
        List.selectedSubcate = $('#subcates select').val();

        //查找该categoryid子分类的属性和品牌并添加
        $.each(List.selectedCate_raw.childrenCategory,function(ks,vs){
            if(vs.categoryid==List.selectedSubcate){
                //更新属性
                $.each(vs.categoryPropertySet, function (kp,vp) {
                    $("#props select").append("<option value='"+ vp.propertyid+"'>"+ vp.propertyName+"</option>");
                    if(kp==0){
                        List.selectedProp_raw = vp;
                    }
                });
                List.selectedProp = $('#props select').val();
                List.selectedSubcate_raw = vs;
                //更新选项
                $.each(List.selectedProp_raw.propertyOptionSet,function(ko,vo){
                    $("#options select").append("<option value='"+ vo.optionid+"'>"+ vo.optionName+"</option>");
                    if(ko==0){
                        List.selectedOption_raw = vo;
                    }
                });
                List.selectedOption = $('#options select').val();

                $.each(vs.brandCategoryMapSet,function(kb,vb){
                    $("#brands select").append("<option value='"+ vb.brandid+"'>"+ vb.brand.brandName+"</option>");
                });
                List.selectedBrand = $('#brands select').val();
            }

        });
    });
    $('#props select').change(function(){
        //更新选项列表
        $("#options select option").remove();

        //获取选中属性id
        List.selectedProp= $('#props select').val();
        $.each(List.selectedSubcate_raw.categoryPropertySet,function(kp,vp){
              if(vp.propertyid==List.selectedProp){
                  $.each(vp.propertyOptionSet,function(ko,vo){
                      $("#options select").append("<option value='"+ vo.optionid+"'>"+ vo.optionName+"</option>");
                      if(ko==0){
                          List.selectedOption_raw = vo;
                      }
                      List.selectedProp_raw = vp;
                      List.selectedOption = $('#options select').val();
                  });
              }
        });
    });
    $('#options select').change(function(){
        //获取选中选项id
        List.selectedOption= $('#options select').val();
    });
    $('#brands select').change(function(){
        //获取选中品牌id
        List.selectedBrand= $('#brands select').val();
    });

});