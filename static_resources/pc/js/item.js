$(document).ready(function(){
    //导航按钮初始化
    $('#nav-menu').smartmenus({
        mainMenuSubOffsetX: -1,
        subMenusSubOffsetX: 10,
        subMenusSubOffsetY: 0
    });

    //click,hover图片轮换
    var toggle_carousel = function(imgurl){
        $("#item_carousels img").remove();
        $("#item_carousels").append("<img src='"+imgurl+"'/>");

    };
//    $("#item_carousels_thumbnail img").click(function(){
//        var imgurl = $(this).attr("src");
//        toggle_carousel(imgurl);
//    });
    $("#item_carousels_thumbnail img").hover(function(){
        var imgurl = $(this).attr("src");
        toggle_carousel(imgurl);
    });
});