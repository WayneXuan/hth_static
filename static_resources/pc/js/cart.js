$(document).ready(function(){
    //导航按钮初始化
    $('#nav-menu').smartmenus({
        mainMenuSubOffsetX: -1,
        subMenusSubOffsetX: 10,
        subMenusSubOffsetY: 0
    });
    //计算#cartitems的高度
    $("#cartitems").css("float","left");//设置成float才能正确计算cartitems的高度
    $("#cartitems").css("height",$("#cartitems").height());
    $("#cartitems").css("float","inherit");
});