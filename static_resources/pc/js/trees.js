/**
 * Created by pcliang on 2014/5/5
 * 树的特征：
 * 分类树（两层）：
 * 根节点下包括子分类和品牌，品牌可以拖动以调整顺序但禁止其他操作：
 * 拖动规则：品牌子节点在其同级可以拖动调整顺序，同级别的分类可以拖动调整顺序，其余拖动操作禁止。
 * 根节点和叶节点击编辑可以修改名称、分类树中显示的品牌不能做修改操作。
 * 分类树上只有叶子节点的品牌映射可以删除，根节点下显示用的品牌列表不能做删除操作
 * 新增节点为选中节点下的子节点，若未选中则新增根节点；

 * 新增品牌需要判断是否已经存在。
 * 品牌树可以修改名称。
 * 品牌树始终显示所有节点。
 * 属性可以修改名称和排序
 * 选项可以修改名称和排序
 * 品牌列表上的品牌可以删除，服务端同时会删除所有分类节点上对此品牌的映射
 */

var setting = {
    edit: {
        enable: true,
        drag:
        {   prev: true,
            next: true,
            inner: false
        },
        showRemoveBtn: false,
        showRenameBtn: false
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        beforeDrag: beforeDrag,
        beforeDrop: beforeDrop,
        onDrop: cTreeOnDrop,
        beforeClick: nodeBeforeClick,
        onClick: nodeAfterClick
    },
    view: {
        selectedMulti: false
    }
};

var zNodes_category = [];   //分类树
var zNodes_categoryC2 = [];   //分类树
var zNodes_brand = [];   //品牌树
var zNodes_property = [];   //属性树
var zNodes_option = [];   //选项树


var treeC1_obj;
var treeB_obj;
var treeC2_obj;
var treeP_obj;
var treeO_obj;



//定义树操作类
function  Tree(treeId) {
    this.treeId = treeId;   //公共变量
//    this.preSelectedNodeId=null;    //点击之前被选中的节点ID
//    this.categories = null;//存储categories json数据
};
Tree.C1_preSelectedNodeId=null; //静态变量：C1 点击之前被选中的节点ID
Tree.C2_preSelectedNodeId=null; //静态变量：C2 点击之前被选中的节点ID
Tree.P_preSelectedNodeId=null; //静态变量：C2 点击之前被选中的节点ID
Tree.categories=null;            // 静态变量：原始数据

Tree.prototype.getTreeId = function(){
    return  this.treeId;
}
Tree.prototype.getSelectedNode =  function(){
    if (this.treeId!=null){
        return $.fn.zTree.getZTreeObj(this.treeId).getSelectedNodes()[0];
    }else{
        return null;
    }
}
Tree.prototype.getBrandCategoryMapid = function(){
    if(this.treeId=="tree_c1" || this.treeId=="tree_c2") {
       return this.getSelectedNode().mapid;
    }else{
        return;
    }
}


function beforeDrag(treeId, treeNodes) {
    for (var i=0,l=treeNodes.length; i<l; i++) {
        dragId = treeNodes[i].pId;
        if (treeNodes[i].drag === false) {
            return false;
        }
    }
    return true;
}
function beforeDrop(treeId, treeNodes, targetNode, moveType) {
    if (targetNode.drop == false || treeId=='tree_b') return false;
    if(targetNode.pId == dragId){
        return true;
    }else{
        return false;
    }
}
function cTreeOnDrop(event, treeId, treeNodes, targetNode, moveType) {
//    alert(treeNodes.length + "," + (targetNode ? (targetNode.tId + ", " + targetNode.name) : "isRoot" ));
//    ajax提交被拖动点的新sort
//    console.log(treeNodes);
//    console.log(treeNodes[0].getNextNode());
//    console.log(treeNodes[0].getPreNode());
    if (targetNode==null) return;
    var newSort;
//    console.log(targetNode.name);
//    console.log(targetNode.sort);
    //获取该节点相邻同级节点的sort并计算该节点新sort值
    if (treeNodes[0].getNextNode()==null || treeNodes[0].getNextNode().sort==undefined){//若是最后同级一个节点
       newSort = treeNodes[0].getPreNode().sort + 5;
    }else if (treeNodes[0].getPreNode()==null || treeNodes[0].getPreNode().sort==undefined){     //若是第一个同级节点
        newSort = treeNodes[0].getNextNode().sort - 5;
    }else{
        newSort = (treeNodes[0].getPreNode().sort+treeNodes[0].getNextNode().sort)/2;
    }



    switch (treeId){
        case 'tree_c1':
            $.ajax({
                url:'/categories/category-all',
                type: 'POST',
                data:{ 'categoryid':treeNodes[0].id,'sort': newSort,'categoryName':treeNodes[0].name},
                success: function(data) {
                    if(data.status==0){
//                $.fn.zTree.getZTreeObj("tree_c1").destroy();
                        getTreeData();
                    }else{
                        jAlert("更新失败...,请重试！");
                    }

                }
            });
        break;
    }

}

function nodeBeforeClick(treeId, treeNode, clickFlag){
    var snode = $.fn.zTree.getZTreeObj(treeId).getSelectedNodes();
    //若该节点不是已选中的节点则直接返回
//    console.log(snode);
    if(snode.length==0 || snode[0].id != treeNode.id) {
        return;
    }else{
//        treeC1_obj.preSelectedNodeId = treeNode.id;
        Tree.C1_preSelectedNodeId = treeNode.id;
    }
}

function nodeAfterClick(event,treeId,treeNode){
    //判断之前选中的节点是否跟当前节点一致
    switch (treeId) {
        case 'tree_c1':
            if (Tree.C1_preSelectedNodeId == treeNode.id){
//        alert(treeNode.id);
                $.fn.zTree.getZTreeObj(treeId).cancelSelectedNode(treeNode);
                Tree.C1_preSelectedNodeId = null;
            }
            break;
        case 'tree_c2':
            pTreeInit();
            oTreeInit();
            break;
        case 'tree_p':
            oTreeInit();
            break;
    }



}


function getTreeData(){
    // make an ajax request --->treeview
    zNodes_category = [];   //分类树C1清空
    zNodes_categoryC2 = [];   //分类树C2清空
    zNodes_brand = [];//品牌树清空

    //构造品牌树
    $.ajax({
        url:'/categories/brand-list',
        type:'GET',
        success: function(data) {

            //********判断该品牌是否已经压入品牌树节点数组************//
            if (data.status == 0) {
                $.each(data.content,function(i,v){
                    zNodes_brand.push({id: v.brandid, name: v.brandName+'('+v.brandEnglish+')', nameEn: v.brandEnglish, icon: "/css/zTreeStyle/img/diy/5.png",drop:false });
                });
//                console.log(zNodes_brand);
                $.fn.zTree.init($("#tree_b"), setting, zNodes_brand);
                var treeObj = $.fn.zTree.getZTreeObj("tree_b").setting.view.selectedMulti = false;
            }
            //*****************************************************//
        }
    });
    //构造分类树C1\C2
    $.ajax({
        url: '/categories/category-tree',
        type: "GET",
        success: function(data) {
            //*********************************************************************//
            //获取树数据，构造树数据
            //pId的赋值
            //*********************************************************************//
            if(data.status==0){
                //存储categories Json数据到Tree.categories
                 Tree.categories = data.content;
//                console.log(Tree.categories);
//                遍历每个data.content数组元素，构造分类树、品牌树、属性树、选项树
//                首先构造分类树，基于两层结构的树，对每个content数组元素childrenCategory进入遍历
                $.each(data.content,function(index1,value1){

                    zNodes_category.push({id:value1.categoryid,pId:0,name:value1.categoryName,open:true,sort:value1.sort});//加入根分类节点
                    zNodes_categoryC2.push({id:value1.categoryid,pId:0,name:value1.categoryName,open:true,sort:value1.sort});//加入根分类节点

                    $.each(value1.brandCategoryMapSet,function(index2,value2){//加入根分类品牌节点
//                        console.log(value2);
                        zNodes_category.push({id:0-value2.brandid,pId:value2.categoryid,mapid:value2.mapid,name:value2.brand.brandName+'('+value2.brand.brandEnglish+')',icon:"/css/zTreeStyle/img/diy/5.png", drop:false});

                    });

                    if (value1.childrenCategory != null) {
                        $.each(value1.childrenCategory,function(index2,value2){

                            zNodes_category.push({id:value2.categoryid,pId:value2.parentCategoryid,name:value2.categoryName,open:true,sort:value2.sort}); //加入子分类节点
                            zNodes_categoryC2.push({id:value2.categoryid,pId:value2.parentCategoryid,name:value2.categoryName,open:true,sort:value2.sort}); //加入子分类节点

                            if (value2.brandCategoryMapSet != null){
                                $.each(value2.brandCategoryMapSet,function(index3,value3){//加入子分类品牌节点
//                            console.log(value3);
                                    zNodes_category.push({id:0-value3.brandid,mapid:value3.mapid,pId:value3.categoryid,name:value3.brand.brandName+'('+value3.brand.brandEnglish+')',icon:"/css/zTreeStyle/img/diy/5.png", drop:false});
                                });
                            }
                        });
                    }
                });
                $.fn.zTree.init($("#tree_c1"), setting, zNodes_category);
                $.fn.zTree.init($("#tree_c2"), setting, zNodes_categoryC2);

                var ztree = $.fn.zTree.getZTreeObj("tree_c2");
                ztree.selectNode(ztree.getNodeByParam("id", Tree.C2_preSelectedNodeId, null));
                pTreeInit();
//                jAlert('hi');
                ztree = $.fn.zTree.getZTreeObj("tree_p");
                ztree.selectNode(ztree.getNodeByParam("id", Tree.P_preSelectedNodeId, null));
                oTreeInit();

            }
        }
    });

}
//构造属性树
var pTreeInit = function () {
   //获取C2树的选中节点，若未选中叶节点则返回；否则从Tree.categories获取数据构造属性树
    zNodes_property=[];//清空
    $.fn.zTree.init($("#tree_p"), setting, zNodes_property);
    if(treeC2_obj.getSelectedNode()==null || treeC2_obj.getSelectedNode().isParent==true) return;
    //从Tree.categories获取categoryPropertySet，将categoryid==treeC2_obj.getSelectedNode().id的都压入树
//    console.log(Tree.categories);

    var scid =  treeC2_obj.getSelectedNode().id;
//    console.log(scid);

    $.each(Tree.categories,function(index,value){
        if(value.categoryid==scid){       //检查根节点
            $.each(value.categoryPropertySet,function(i,v){
                    zNodes_property.push({id: v.propertyid,name: v.propertyName});
            });
        }else{                      //检查叶节点
            if(value.childrenCategory.length>0){
                $.each(value.childrenCategory,function(i,v){
                   if(v.categoryid==scid){
                       $.each(v.categoryPropertySet,function(index,value){
                           zNodes_property.push({id: value.propertyid,name: value.propertyName,categoryid:scid});
                       });
                   }
                });
            }
        }
    });

    $.fn.zTree.init($("#tree_p"), setting, zNodes_property);
}
//构造选项树
var oTreeInit = function () {
    //获取P树的选中节点，若未选中叶节点则返回；否则从Tree.categories获取数据构造选项树
    zNodes_option=[];//清空
    $.fn.zTree.init($("#tree_o"), setting, zNodes_option);

//    $.fn.zTree.init($("#tree_o"), setting, zNodes_option);
    if(treeP_obj.getSelectedNode()==null) return;
    //从Tree.categories获取propertyOptionSet，将propertyid==treeP_obj.getSelectedNode().id的都压入树
//    console.log(Tree.categories);

    var scid =  treeP_obj.getSelectedNode().id;

    $.each(Tree.categories,function(index,value){
        if(value.childrenCategory.length==0){
            //检查根节点
            $.each(value.categoryPropertySet,function(ix,ve){
                if(ve.propertyid==scid){
                    $.each(ve.propertyOptionSet,function(i1,v1){
                        zNodes_option.push({id: v1.optionid,name: v1.optionName,propertyid:scid});
                    });
                }
            });
        }else{
            //检查叶节点
            $.each(value.childrenCategory,function(i,v){

                $.each(v.categoryPropertySet,function(ix,ve){
                    if(ve.propertyid==scid){
                        $.each(ve.propertyOptionSet,function(i1,v1){
                            zNodes_option.push({id: v1.optionid,name: v1.optionName,propertyid:scid});
                        });
                    }
                });

            });
        }
    });

    $.fn.zTree.init($("#tree_o"), setting, zNodes_option);
}


$(document).ready(function(){
    //导航按钮初始化
    $('#nav-menu').smartmenus({
        mainMenuSubOffsetX: -1,
        subMenusSubOffsetX: 10,
        subMenusSubOffsetY: 0
    });

    getTreeData();
    //分配树实例
    treeC1_obj = new Tree('tree_c1');
    treeB_obj = new Tree('tree_b');
    treeC2_obj = new Tree('tree_c2');
    treeP_obj = new Tree('tree_p');
    treeO_obj = new Tree('tree_o');

    //分类树操作事件绑定
    $("#treeC1_new").bind("click",function(){  //分类树新增
        jPrompt('请输入新分类,未选中默认增加根分类:','','新增分类', function(r) {
            if (r) {
                //首先提示非法字符，通过检查后获取根分类id,提交ajax Post
                var pattern = new RegExp(/[@#\$%\^&\*]+/g);
                if ( r=='' || pattern.test(r)) {
                    jAlert('请输入分类名或者去掉非法字符！');
                }else{
                    var sNode=treeC1_obj.getSelectedNode();
                    if (sNode==null){  //新增根分类
                        $.ajax({
                            url:'/categories/category-all',
                            type: 'POST',
                            data:{ 'categoryName':r,'parentCategoryid':null},
                            success: function(data) {
                                if(data.status==0){
                                    getTreeData();
                                }else{
                                    jAlert("更新失败...,请重试！");
                                }

                            }
                        });
                    }else if(sNode.pId==null){ //新增子分类
                            $.ajax({
                                url:'/categories/category-all',
                                type: 'POST',
                                data:{ 'categoryName':r,'parentCategoryid':sNode.id},
                                success: function(data) {
                                    if(data.status==0){
                                        getTreeData();
                                    }else{
                                        jAlert("更新失败...,请重试！");
                                    }

                                }
                            });
                    }else{
                        jAlert("子分类无法新增下级分类了...");
                    }
                }
            }

        });
    });

    $("#treeC1_del").bind("click",function(){  //分类树删除
            //获取选中的分类id,提交ajax Post
            var sNode=treeC1_obj.getSelectedNode();
            if (sNode==null){  //未选中
                jAlert('请选中后再删除！');
                return;
            }else{ //删除分类
                jConfirm('确定删除吗？','删除',function(r){
                    if(r){
                        if(sNode.getParentNode()!=null && sNode.getParentNode().pId != null && sNode.drop==false){    //若是品牌节点
                            $.ajax({
                                url:'/categories/brand-category',
                                type: 'POST',
                                data:{ 'mapid':sNode.mapid, 'isdeleted':true},
                                success: function(data) {
                                    if(data.status==0){
                                        getTreeData();
                                    }else{
                                        jAlert("删除失败...,请重试！");
                                    }

                                }
                            });
                        }else if(sNode.id>0){   //若是分类节点
                            $.ajax({
                                url:'/categories/category-all',
                                type: 'POST',
                                data:{ 'categoryid':sNode.id, 'isdeleted':true},
                                success: function(data) {
                                    if(data.status==0){
                                        getTreeData();
                                    }else{
                                        jAlert("删除失败...,请重试！");
                                    }

                                }
                            });
                        }else{
                            jAlert('无法删除根分类下的品牌','删除提示');
                        }

                    }
                });

            }
        });

    $("#treeC1_mod").bind("click",function(){  //分类树修改
        //获取选中的分类id,提交ajax Post
        var sNode=treeC1_obj.getSelectedNode();
        if (sNode==null){  //未选中
            jAlert('请选中后再修改！');
        }else{ //先获取新的分类名然后提交
            if(sNode.id<0) {
                jAlert('请在右边修改品牌','修改提示');
                return;
            }
            jPrompt('请输入新分类名：',sNode.name,'修改分类名称',function(r){
                if (r){

                        var pattern = new RegExp(/[@#\$%\^&\*]+/g);
                        if ( r=='' || pattern.test(r)) {
                            jAlert('请输入分类名或者去掉非法字符！');
                        }else{
                            $.ajax({
                                url:'/categories/category-all',
                                type: 'POST',
                                data:{ 'categoryid':sNode.id, 'categoryName':r},
                                success: function(data) {
                                    if(data.status==0){
                                        getTreeData();
                                    }else{
                                        jAlert("修改失败...,请重试！");
                                    }

                                }
                            });
                        }
                    }

            });
        }
    });

    //品牌树操作事件绑定
    $("#treeB_new").bind("click",function(){  //品牌树新增
        jPrompt('请输入新品牌:','格式：中文名,EnglishName','新增品牌', function(r) {
            if (r) {
                //首先提示非法字符，通过检查后获取根分类id,提交ajax Post
                var pattern = new RegExp(/[@#\$%\^\*]+/g);
                if ( r=='' || pattern.test(r)) {
                    jAlert('请输入品牌名或者去掉非法字符！');
                }else{
                        $.ajax({
                            url:'/categories/brand-all',
                            type: 'POST',
                            data:{ 'brandName': r.split(',')[0],'brandEnglish':r.split(',')[1]},
                            success: function(data) {
                                if(data.status==0){
                                    getTreeData();
                                }else{
                                    jAlert("更新失败...,请重试！");
                                }

                            }
                        });
                    }
                }
        });
    });

    $("#treeB_del").bind("click",function(){  //品牌树删除
        //获取选中的id,提交ajax Post
        var sNode=treeB_obj.getSelectedNode();
        if (sNode==null){  //未选中
            jAlert('请选中后再删除！');
        }else{ //删除品牌
            jConfirm('确定删除吗？','删除',function(r){
                if(r){
                        $.ajax({
                            url:'/categories/brand-all',
                            type: 'POST',
                            data:{ 'brandid':sNode.id, 'isdeleted':true},
                            success: function(data) {
                                if(data.status==0){
                                    getTreeData();
                                }else{
                                    jAlert("删除失败...,请重试！");
                                }

                            }
                        });
                }

            });

        }
    });

    $("#treeB_mod").bind("click",function(){  //品牌树修改
        //获取选中的id,提交ajax Post
        var sNode=treeB_obj.getSelectedNode();
        if (sNode==null){  //未选中
            jAlert('请选中后再修改！');
        }else{ //先获取新的品牌名然后提交
            jPrompt('请输入修改品牌名：',sNode.name,'修改品牌名称',function(r){
                if (r){
                    var pattern = new RegExp(/[@#\$%\^\*]+/g);
                    if ( r=='' || pattern.test(r)) {
                        jAlert('请输入品牌名或者去掉非法字符！');
                    }else{
                        $.ajax({
                            url:'/categories/brand-all',
                            type: 'POST',
                            data:{ 'brandid':sNode.id, 'brandName': r.split(',')[0],'brandEnglish': r.split(',')[1]},
                            success: function(data) {
                                if(data.status==0){
                                    getTreeData();
                                }else{
                                    jAlert("修改失败...,请重试！");
                                }

                            }
                        });
                    }
                }

            });
        }
    });

    //添加删除品牌分类映射
    //添加映射：如无选中分类或品牌则提示并返回；若有则检查是否已经存在该映射，若没有则添加，成功后提示；
    $("#moveBrand_left").bind("click",function(){
        var sbNode=treeB_obj.getSelectedNode();
        var scNode=treeC1_obj.getSelectedNode();

       if (sbNode==null || scNode==null) {
           jAlert('请选择分类和品牌添加映射！');
           return;
       }else{
//           console.log(sbNode);
//           console.log(scNode);
           //若是根分类且有子分类则无法添加
//           console.log(scNode);
           if (scNode.pId==null && scNode.children!=undefined){
               jAlert('请在子分类添加品牌！');
               return;
           }
           var bnames = [];
           if (scNode.isParent){
               $.each(scNode.children,function(i,v){
                   bnames.push(v.name);
               });
           }else if(scNode.id<0){
               jAlert('品牌节点无法添加子品牌！');
               return;
           }

           if( $.inArray(sbNode.name,bnames)!=-1 ){
               jAlert('该分类已经存在该品牌无须重复添加！');
           }else{
               $.ajax({
                   url:'/categories/brand-category',
                   type: 'POST',
                   data:{ 'brandid':sbNode.id, 'categoryid': scNode.id  },
                   success: function(data) {
                       if(data.status==0){
                           getTreeData();
                       }else{
                           jAlert("添加失败...,请重试！");
                       }

                   }
               });
           }
       }
    });

    //删除映射：忽略未选中映射的操作。如果不是叶子节点下面的品牌则提示无法删除。删除成功后提示。
    $("#moveBrand_right").bind("click",function(){
        var scNode=treeC1_obj.getSelectedNode();

        if (scNode==null || scNode.id>0) {
            jAlert('请在分类树选择品牌以删除映射！');
            return;
        }else{
            //判断选中的节点是否为根分类下的品牌节点，若是则需要判断根分类下是否有子分类，若没有才可以删除。
            var hassubCatogory=false;
            $.each(scNode.getParentNode().children,function(i,v){
                if (v.id>0) {
                    hassubCatogory=true;
                    return;
                }
            });
            if(hassubCatogory){
                jAlert('请在子分类中删除该品牌！');
                return;
            }
//            console.log(scNode);
            jConfirm('确定删除吗？','删除',function(r) {
                if (r) {
                    $.ajax({
                        url:'/categories/brand-category',
                        type: 'POST',
                        data:{ 'mapid':treeC1_obj.getBrandCategoryMapid(), 'isdeleted': true  },
                        success: function(data) {
                            if(data.status==0){
                                getTreeData();
                            }else{
                                jAlert("删除失败...,请重试！");
                            }
                        }
                    });
                }
            });
        }
    });

    //新增属性
    $("#treeP_new").bind("click",function(){
        //若tree C2未选中叶节点则提示需选中子分类，返回。
        var scNode=treeC2_obj.getSelectedNode();
        if (scNode==null || (scNode.isParent==true && scNode.children.length>0) ){
            jAlert("请选中子分类增加属性！");
        }else{
            Tree.C2_preSelectedNodeId = scNode.id;
            jPrompt('请输入新属性:','属性名称','新增属性', function(r) {
                if (r) {
                    //首先提示非法字符，通过检查后获取根分类id,提交ajax Post
                    var pattern = new RegExp(/[@#\$%\^&\*]+/g);
                    if ( r=='' || pattern.test(r)) {
                        jAlert('请输入属性名或者去掉非法字符！');
                    }else{
                        $.ajax({
                            url:'/categories/category-property',
                            type: 'POST',
                            data:{ 'propertyName': r,'categoryid':scNode.id},
                            success: function(data) {
                                if(data.status==0){
                                    getTreeData();
                                    //$.fn.zTree.getZTreeObj("tree_c2").selectNode(scNode);
//                                    jAlert("新增成功！");
                                }else{
                                    jAlert("新增失败...,请重试！");
                                }

                            }
                        });
                    }
                }
            });
        }

    });

    //删除属性
    $("#treeP_del").bind("click",function(){
        //检查属性树是否有选中节点，否则返回。若有则获取其propertyid发送ajax
        if (treeP_obj.getSelectedNode()==null) {
            jAlert("请选中属性以删除！");
            return;
        }
        var pid=treeP_obj.getSelectedNode().id;
        jConfirm('确定删除吗？','删除',function(r) {
            if (r) {
                $.ajax({
                    url: '/categories/category-property',
                    type: 'POST',
                    data: { 'propertyid': pid, 'isdeleted': true},
                    success: function (data) {
                        if (data.status == 0) {
                            getTreeData();
                        } else {
                            jAlert("删除失败...,请重试！");
                        }

                    }
                });
            }
        });
    });
    //修改属性
    $("#treeP_mod").bind("click",function(){
        //检查属性树是否有选中节点，否则返回。若有则获取其propertyName和propertyid发送ajax
        if (treeP_obj.getSelectedNode()==null) {
            jAlert("请选中属性以修改！");
            return;
        }
        var pid=treeP_obj.getSelectedNode().id;
        var pname= treeP_obj.getSelectedNode().name;
        jPrompt('请输入新属性名：',pname,'修改属性',function(r){
            if (r){
                var pattern = new RegExp(/[@#\$%\^&\*]+/g);
                if ( r=='' || pattern.test(r)) {
                    jAlert('请输入分类名或者去掉非法字符！');
                }else{
                    $.ajax({
                        url:'/categories/category-property',
                        type: 'POST',
                        data:{ 'propertyid':pid, 'propertyName':r},
                        success: function(data) {
                            if(data.status==0){
                                getTreeData();
                            }else{
                                jAlert("修改失败...,请重试！");
                            }
                        }
                    });
                }
            }

        });
    });
    //新增选项
    $("#treeO_new").bind("click",function(){
        //若tree P未选中叶节点则提示需选中子分类，返回。
        var spNode=treeP_obj.getSelectedNode();
        if (spNode==null){
            jAlert("请选中属性添加选项！");
            return;
        }
        Tree.P_preSelectedNodeId = spNode.id;
        Tree.C2_preSelectedNodeId = treeC2_obj.getSelectedNode().id;
        jPrompt('请输入新选项:','选项名称','新增选项', function(r) {
            if (r) {
                //首先提示非法字符，通过检查后获取根分类id,提交ajax Post
                var pattern = new RegExp(/[@#\$%\^&\*]+/g);
                if (r == '' || pattern.test(r)) {
                    jAlert('请输入属性名或者去掉非法字符！');
                    return;
                }
                $.ajax({
                    url:'/categories/property-option',
                    type: 'POST',
                    data:{ 'optionName': r,'propertyid':spNode.id},
                    success: function(data) {
                        if(data.status==0){
                            getTreeData();
                        }else{
                            jAlert("新增失败...,请重试！");
                        }

                    }
                });
            }
        });
    });

    //删除选项
    $("#treeO_del").bind("click",function(){
       // 检查选项树是否有选中节点，否则返回。若有则获取其optionid发送ajax
        if (treeO_obj.getSelectedNode()==null) {
            jAlert("请选中选项以删除！");
            return;
        }
        Tree.P_preSelectedNodeId = treeP_obj.getSelectedNode().id;
        Tree.C2_preSelectedNodeId = treeC2_obj.getSelectedNode().id;
        var oid=treeO_obj.getSelectedNode().id;
        jConfirm('确定删除吗？','删除',function(r) {
            if (r) {
                $.ajax({
                    url: '/categories/property-option',
                    type: 'POST',
                    data: { 'optionid': oid, 'isdeleted': true},
                    success: function (data) {
                        if (data.status == 0) {
                            getTreeData();
                        } else {
                            jAlert("删除失败...,请重试！");
                        }

                    }
                });
            }
        });
    });
    //修改选项
    $("#treeO_mod").bind("click",function() {
        //检查选项树是否有选中节点，否则返回。若有则获取其optionName和optionid发送ajax
        if (treeO_obj.getSelectedNode()==null) {
            jAlert("请选中选项以修改！");
            return;
        }
        Tree.P_preSelectedNodeId = treeP_obj.getSelectedNode().id;
        Tree.C2_preSelectedNodeId = treeC2_obj.getSelectedNode().id;
        var oid=treeO_obj.getSelectedNode().id;
        var oname= treeO_obj.getSelectedNode().name;
        jPrompt('请输入新选项名：',oname,'修改选项',function(r){
            if (r){
                var pattern = new RegExp(/[@#\$%\^&\*]+/g);
                if ( r=='' || pattern.test(r)) {
                    jAlert('请输入选项名或者去掉非法字符！');
                }else{
                    $.ajax({
                        url:'/categories/property-option',
                        type: 'POST',
                        data:{ 'optionid':oid, 'optionName':r},
                        success: function(data) {
                            if(data.status==0){
                                getTreeData();
                            }else{
                                jAlert("修改失败...,请重试！");
                            }
                        }
                    });
                }
            }

        });
    });
});