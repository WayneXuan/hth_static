<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<#import "/spring.ftl" as spring />
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-首页</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>
    <div id="member_info">
    <form action="/member-mine/member-info" method="post">
        <p>用户名：${member.passportid!}</p>
        <p>姓名：<input type="text" name="name" value="${member.name!}"/> </p>
        <p>联系方式：<input type="text" name="mobile" value="${member.mobile!}"/>></p>
        <p>邮箱：<input type="text" name="mail" value="${member.mail!}"/></p>
        <input type="submit" value="更新"/>
    </form>
    <form action="/member-mine/member-password" method="post">
        <p>旧密码：<input type="password" name="orlpassword" value=""/><#if passwordWrong??><b>密码错误</b></#if></p>
        <p>新密码：<input type="password" name="password"/>
        <@spring.bind "member.password" />
        <@spring.showErrors "<br/>"/></p>
        <p><input type="submit" value="更改"/></p>
    </form>
</div>
    </div>
</body>
</html>