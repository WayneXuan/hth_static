<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html;image/jpeg;image/jpg;image/png;charset=UTF-8">
    <title>管理客户</title>
    <link rel="stylesheet" href="/css/pcStyle_backend_categories.css" type="text/css">
    <link rel="stylesheet" href="/css/jquery.alerts.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_common.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.alert.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <#--<script type="text/javascript" src="/highcourt/js/item-edit.js"></script>-->
    <script type="application/javascript">
        function response(data,operate) {
            if("ban"==operate) {

            }
        }

        $(function(){
            $("a[name^='ban_']").each(function(){

                $(this).click(function(){
                    var memberid = $(this).attr("name").substring(4);var data_send;

                    if($(this).html()=="禁用账户") {
                        data_send = {banflag:1};
                    }
                    else{
                        data_send = {banflag:0};
                    }

                    $.ajax({
                        url:"/members/member-ban-"+memberid,
                        type:"post",
                        data:data_send,
                        dataType: "json",
                        success:function(data) {
                            if(data.status==0) {
                                if(data_send.banflag==1) {
                                    $("a[name='ban_"+memberid+"']").html("启用账户");
                                }
                                else{
                                    $("a[name='ban_"+memberid+"']").html("禁用账户");
                                }
                            }
                        }
                    });

                });
            });



        });

    </script>
</head>
<body>
<div class="page">
<#include "backheader.ftl"/>
<#include "leftnav.ftl"/>
    <div class="rightcontent">
        <div id="filterForm">
        <form action="/members" method="get">
            <label>用户名</label><input name="passportid" value="${(pageModel.filterMap['passportid'])!}"/>
            <label>开始时间</label><input name="begintime" value="${(pageModel.filterMap['begintime']?date)!}"/>
            <label>结束时间</label><input name="endtime" value="${(pageModel.filterMap['endtime']?date)!}"/>
            <input type="submit" value="提交">
        </form>
        </div>
        <div id="members_list">
            <table style="width:600px">
                <thead align="left" ><tr><th>用户名</th><th>姓名</th><th>联系方式</th><th>注册时间</th><th>上级代理</th><th>操作</th></tr></thead>
                <tbody>
                    <#if pageModel??&&pageModel.pageList??>
                        <#list pageModel.pageList as member>
                            <tr style="margin-bottom:10px"><td>${member.passportid!}</td><td>${member.name!}</td><td>${member.mobile!}</td><td>${member.registerTime?datetime!}</td><td>${(member.authorised.authorisorMember.name)!}</td>
                                <td><#if member.status=="Normal"><a name="ban_${member.memberid}" href="javascript:void(0)" >禁用账户</a>
                                <#else ><a name="ban_${member.memberid}" href="javascript:void(0)" >启用账户</a></#if></td></tr>
                        </#list>
                    </#if>
                </tbody>
            </table>
        </div>
        <div id="pagination">
        <#if pageModel??&&pageModel.pageNumber??&&(pageModel.pageNumber>1)>
        <a href="/members?pagenumber=${(pageModel.pageNumber-1)!}<#list pageModel.filterMap?keys as curKey>
        &${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}<#else>${pageModel.filterMap[curKey]}</#if>
        </#list>">上一页</a> </#if>
        第${(pageModel.pageNumber)!}/${(pageModel.totalPage)!}页
        <#if pageModel??&&pageModel.pageNumber??&&pageModel.totalPage??&&(pageModel.pageNumber<pageModel.totalPage)>
        <a href="/members?pagenumber=${(pageModel.pageNumber+1)!}<#list pageModel.filterMap?keys as curKey>
        &${curKey}=<#if pageModel.filterMap[curKey]?is_date>${pageModel.filterMap[curKey]?date}<#else>${pageModel.filterMap[curKey]}</#if>
        </#list>">下一页</a> </#if>
        </div>
    </div>
</div>
</body>
</html>
