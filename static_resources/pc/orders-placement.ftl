<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-首页</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
    <script type="application/javascript">
        var selectedConsigneeid = "";
        $(function() {
            $("input[name='consignee_select']").change(function () {
                $("input[name='consigneeid']").val($(this).val());
            });
            $("input[name='consignee_select']").each(function() {
                if($(this).prop("checked")==true) {
                    selectedConsigneeid = $(this).val();
                }
            });

            if(selectedConsigneeid=="") {
                var $firstRadio = $("input[name='consignee_select']:first");
                if($firstRadio!=null) {
                    $firstRadio.prop("checked", true);
                    selectedConsigneeid = $firstRadio.val();
                }
            }


            $("input[name='consigneeid']").val(selectedConsigneeid);

            function setDefault()  {$("a[name^='default_']").each(function () {
                $(this).click(function(){
                    var consigneeid = $(this).attr("name").substring(8);
                    var data_send = {"consigneeid":consigneeid,"operate":"default"};


                    $.ajax({
                        url:"/consignee/address-config",
                        type:"post",
                        data:data_send,
                        dataType: "json",
                        success:function(data) {
                            if(data.status==0) {
                                $("td[name^='default_']").each(function () {
                                    var currentConsigneeid = $(this).attr("name").substring(8);
                                    if(currentConsigneeid!=consigneeid) {
                                        $(this).html('<a name="default_'+currentConsigneeid+'" href="javascript:void(0)">设置默认地址</a>');
                                    }
                                    else {
                                        $(this).html('这是默认地址');
                                    }
                                });
                                setDefault();
                            }
                        }
                    });

                });
            });}
            setDefault();
        });
    </script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>
    <div id="totalorders">
        <div>
            <p id="orders_addrslabel">收件地址</p>
            <a id="orders_addnewaddr" href="/consignee">+添加新地址</a>
            <div>
                <table id="orders_currentaddrs">
                    <thead>
                    <tr>
                        <th>-</th><th>收件人</th><th>市区县</th><th>地址</th><th>邮编</th><th>手机号码</th><th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#if consigneeList??>
                        <#list consigneeList as consignee>
                        <tr>
                            <td><input type="radio" name="consignee_select" value="${consignee.consigneeid!}" <#if consignee.isdefault??&&consignee.isdefault==true>checked="checked" </#if></td>
                            <td>${consignee.consigneeName!}</td>
                            <td>${(addressProvinceMap[consignee.provinceid?string].provinceName)!}
                                -${(addressCityMap[consignee.cityid?string].cityName)!}
                                -${(addressAreaMap[consignee.areaid?string].areaName)!}</td>
                            <td>${consignee.consigneeAddress!}</td>
                            <td>${consignee.postcode!}</td>
                            <td>${consignee.consigneePhone!}</td>
                            <td name="default_${consignee.consigneeid}"><#if consignee.isdefault??&&consignee.isdefault==true>这是默认地址<#else><a name="default_${consignee.consigneeid}" href="javascript:void(0)">设置默认地址</a> </#if></td>
                        </tr>
                        </#list>
                    </#if>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="orders_block">

            <table id="cart_table">
                <#--<thead>-->
                <#--<tr>-->
                    <#--<th>商品</th><th>商品价格</th><th>数量</th><th>总价</th>-->
                <#--</tr>-->
                <#--</thead>-->
                <tbody>
                <#assign totalPrice=0>
                <#list cartList as cart>
                <tr id="row_${cart_index}">
                    <td>
                        <input id="item_${cart.itemid}" type="hidden" name="carts[${cart_index}].itemid" value="${cart.itemid}"/>

                        <#if cart.itemShow.avatarPath??>
                            <img width="60" height="90" src="${cart.itemShow.avatarPath}"/>
                        </#if>
                    </td>
                    <td>
                        ${cart.itemShow.itemName!}
                    </td>
                    <td>
                        <#if cart.itemShow.propertyOptionList??>
                            <#list cart.itemShow.propertyOptionList as option>
                            ${(option.categoryProperty.propertyName)!}：${(option.optionName)!}
                            </#list>
                        </#if>
                    </td>
                    <td>
                    价格：${cart.itemShow.price!}
                    </td>
                    <td>
                    数量：${cart.count}
                    </td>
                    <td>
                    小计：${cart.itemShow.price?double*cart.count?int}
                <#assign totalPrice=(totalPrice+cart.itemShow.price?double*cart.count?int)>
                    </td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
        <div>
            <#--<p>订单结算</p>-->
            <div>
                <p id="orders_totalprice">订单总计：${totalPrice!}</p>
                <form action="/orders-mine/orders-placement" method="post">
                    <input type="hidden" name="timestamp" value="${(cartList[0].operateTime?string('yyyyMMddHHmmsssss'))!}">
                    <input type="hidden" name="consigneeid" value="3">
                    <p>留言：<textarea name="message" id="orders_leftmessage"></textarea></p>
                    <p><input id="orders_submit" type="submit" value="提交"/></p>
                </form>
            </div>
        </div>
    </div>

</div>
</body>
</html>