<#import "/spring.ftl" as spring />
    <script type="application/javascript" src="/js/jquery-2.1.0.min.js"></script>

register page
<div id="registerForm"><form action="/members/member-register" method="post">

    <div>用户名<input name="passportid" value="${(member.passportid)!}"><#if passportidUniqueError??&&passportidUniqueError==true><b>用户名已存在！</b><br/></#if>
    <@spring.bind "member.passportid" />
    <@spring.showErrors "<br/>"/> </div>
    <div>密码<input name="password" value="" type="password"><@spring.bind "member.password" />
    <@spring.showErrors "<br/>"/></div>
    <div>确认密码<input name="password_confirm" value=""  type="password"></div>

    <div>姓名    <input name="name" value="${(member.name)!}">   <@spring.bind "member.name" />
    <@spring.showErrors "<br/>"/> </div>

    <div>联系方式<input name="mobile" value="${(member.mobile)!}"><@spring.bind "member.mobile" />
    <@spring.showErrors "<br/>"/></div>
    <div>邮箱<input name="mail" value="${(member.mail)!}"><@spring.bind "member.mail" />
    <@spring.showErrors "<br/>"/></div>
    <div>邀请码<input name="invitationcode" value="${invitationCode!}">
        <#if invitationError??&&invitationError==true><b>邀请码不存在或者已经过期</b></#if></div>

    <div id="registerError"><#if registerError??&&registerError==true><b>注册失败请重试</b></#if></div>
    <div><input type="submit" value="提交"></div>
</form></div>


