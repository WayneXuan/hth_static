<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-首页</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
    <#include "header.ftl"/>
    <div id="selection">
        <div id="children_category">
            <ul>
                <li>${category.categoryName!}
                    <ul>
                    <#if category.childrenCategory??>
                        <#list category.childrenCategory as childCategory>
                            <li><a href="">${childCategory.categoryName!}</a> </li>
                        </#list>
                    </#if>
                    </ul>
                </li>

            </ul>


        </div>
        <div id="brand_list">
        <#if category.brandCategoryMapSet??>
            <#list category.brandCategoryMapSet as branMap>
                <div><a href=""><img src="/img/brand/${(branMap.brand.brandEnglish)!}_p.jpg" alt="${(branMap.brand.brandName)!}"/></a></div>
            </#list>
        </#if>
        </div>
        <div id="item_category_list">
        <#if category.childrenCategory??>
            <#list category.childrenCategory as childCategory>
                <div name="item_list">
                    <div name="items">
                        <#if itemShowListArray??&&itemShowListArray[childCategory_index]??>
                            <#list itemShowListArray[childCategory_index] as item>
                                <div>
                                    <#if item.avatarPath??>
                                        <img src="${item.avatarPath}"/>
                                    </#if>
                                ${item.itemName}
                                ${item.price}
                                </div>
                            </#list>
                        </#if>
                    </div>
                </div>
            </#list>
        </#if>

        </div>
    </div>

    <#include "footer.ftl"/>

</div>
</body>
</html>