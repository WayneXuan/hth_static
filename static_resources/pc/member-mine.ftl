<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>海天汇-账户信息</title>
    <link rel="stylesheet" href="/css/header.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">

    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
</head>
<body>
<div class="page">
<#include "header.ftl"/>
<#include "member-leftnav.ftl"/>
    <div id="member_info">
    <#if updated??&&updated==true><label>更新成功</label><#elseif updated??&&updated==false><label>更新失败</label></#if>
        <form action="/member-mine/member-info" method="post"
              style="float: left; margin-right: 100px;padding-top: 40px;">
            <p>用户名：${member.passportid!}</p>

            <p>姓名：<input type="text" name="name" value="${member.name!}"/></p>

            <p>联系方式：<input type="text" name="mobile" value="${member.mobile!}"/></p>

            <p>邮箱：<input type="text" name="mail" value="${member.mail!}"/></p>
            <input type="submit" value="更新"/>
        </form>
        <form action="/member-mine/member-password" method="post" style="padding-top: 40px;">
            <p>旧密码：<input type="password" name="orlpassword" value=""/></p>

            <p>新密码：<input type="password" name="password"/></p>

            <p><input type="submit" value="更改"/></p>
        </form>
    </div>
<#include "footer.ftl"/>
</div>
</body>
</html>