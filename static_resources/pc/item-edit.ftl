<#import "/spring.ftl" as spring />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>
<head>
    <meta http-equiv="content-type" content="text/html;image/jpeg;image/jpg;image/png;charset=UTF-8">
    <title>编辑商品</title>
    <link rel="stylesheet" href="/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_categories.css" type="text/css">
    <link rel="stylesheet" href="/css/jquery.alerts.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-core-css.min.css" type="text/css">
    <link rel="stylesheet" href="/css/sm-simple.min.css" type="text/css">
    <link rel="stylesheet" href="/css/pcStyle_backend_common.css" type="text/css">
    <link rel="stylesheet" href="/js/uploadify.css" type="text/css">


    <script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.alert.js"></script>
    <script type="text/javascript" src="/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="/js/jquery.uploadify.min.js"></script>
    <script type="text/javascript" src="/highcourt/js/item-edit.js"></script>
    <script type="application/javascript">
        $(function() {
            $("select[name='statusSelect']").change(function () {
                location.href="/items/item-status-"+$(this).attr("id").substr(13)+"?status="+$(this).val();
            });
        });
    </script>
</head>
<body>
<div class="page">
<#include "backheader.ftl"/>
<#include "leftnav.ftl"/>
    <div class="rightcontent">
        封面图片：<br><input type="file" name="avatarpicture" id="avatarpicture"/>
        <div id="avatar_list"></div>
    <#--<div class="sep_line"></div>-->
        <br><br>
        跑马灯图片：<br><input type="file" name="carouselspicture" id="carouselspicture"/>
        <div id="carousels_list"></div>
        <br>
        <input id="picurl" type="hidden" value="/items/itemid-${item.itemid}/pictures"/>
        <form id="item_edit_form" action="/items/item-edit-${item.itemid}" method="post">
            <div id="hidden_avatars">
                <input type="hidden" name="avatarPictureids" id="avatarPictureids"/>
            </div>
            <div id="hidden_carousels"></div>

            <div class="buhuo">推荐<input type="checkbox" name="recommend"
                         <#if item??&&item.recommend??&&item.recommend==true>checked="checked" </#if>/></div>
            <div class="buhuo">独家<input type="checkbox" name="exclusive"
                         <#if item??&&item.exclusive??&&item.exclusive==true>checked="checked" </#if>/></div>
            <div>时尚<input type="checkbox" name="fashionable"
                                 <#if item??&&item.fashionable??&&item.fashionable==true>checked="checked" </#if>/></div>
            <br>
            <div id="cates">分类：
                <#list item.categoryList as category>
                    <#--<label>${category.categoryName}</label>-->
                    <#--<input type="hidden" name="orig_categoryName" value="${category.categoryName}"/>-->
                        <input type="hidden" name="orig_categoryids" value="${category.categoryid}"/>
                </#list>
                <select name="categoryids"></select></div>
            <div id="subcates"><select name="categoryids"></select></div>
            <div id="brands">品牌：
                <#--<label>${(item.brand.brandName)!}</label>-->
                <select name="brandid"></select>
                <input type="hidden" name="orig_brandid" value="${item.brandid!}"/>
            </div>
            <div id="propopts">
                <div class="popairs"></div>
            <#list item.propertyOptionList as option>
                    <#--<label>${(option.categoryProperty.propertyName)!}：${option.optionName}</label>-->
                    <input type="hidden" name="orig_optionids" value="${option.optionid}">
                </#list>
            </div>

            <div id="price">价格<input type="text" name="price" value="${item.price!}"></div>
            <div id="itemname">物品名称<input type="text" name="itemName" value="${item.itemName!}"/></div>
            <div id="itemdesc">物品描述<textarea name="description">${item.description!}</textarea></div>
            <div><input type="submit" value="提交"/></div>

        </form>
    </div>
</div>
</body>
</html>