package com.hth.lh.web.util;

import com.hth.lh.web.model.DeviceKey;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Bo Liu
 * Date: 2014/5/3
 * Time: 11:49
 * To change this template use File | Settings | File Templates.
 */
public class ImageTool {

    public static final String IMG_AVATAR = "avatar";
    public static final String IMG_CAROUSELS = "carousels";
    public static final String imageWebPath = "/img";
    public static final String imageFilePath = "/WEB-INF/upload_img";
    public static final String imageFilePcSuffix = "_p";
    public static final String imageFileMobileSuffix = "_m";
    public static final String imageFileFormatSuffix = ".jpg";
    public static final String imageFileFormat = "jpg";

    private static List<String> imageMIMEList = Arrays.asList("image/jpeg","image/jpg","image/png");

    public static Set<String> imageMIME = new HashSet<>(imageMIMEList);




    private static final int width_avatar_pc = 200;
    private static final int height_avatar_pc = 300;
    private static final int width_avatar_mobile = 100;
    private static final int height_avatar_mobile = 150;
    private static final int width_carousel_pc = 400;
    private static final int height_carousel_pc = 300;
    private static final int width_carousel_mobile = 200;
    private static final int height_carousel_mobile = 150;



    public static Map<String,BufferedImage> createDeviceImages(BufferedImage srcImage,String imageType) {
        Map<String,BufferedImage> bufferedImageMap = new HashMap<>(2);
        if (imageType!=null&&imageType.equals(IMG_AVATAR)) {
            BufferedImage pcAvatarImage = getScaledInstance(srcImage,width_avatar_pc,height_avatar_pc);
            BufferedImage mobileAvatarImage = getScaledInstance(srcImage,width_avatar_mobile,height_avatar_mobile);
            bufferedImageMap.put(DeviceKey.DEVICE_PC,pcAvatarImage);
            bufferedImageMap.put(DeviceKey.DEVICE_MOBILE,mobileAvatarImage);
        }
        else if (imageType!=null&&imageType.equals(IMG_CAROUSELS)){
            BufferedImage pcCarouselImage = getScaledInstance(srcImage,width_carousel_pc,height_carousel_pc);
            BufferedImage mobileCarouselImage = getScaledInstance(srcImage,width_carousel_mobile,height_carousel_mobile);
            bufferedImageMap.put(DeviceKey.DEVICE_PC,pcCarouselImage);
            bufferedImageMap.put(DeviceKey.DEVICE_MOBILE,mobileCarouselImage);
        }

        return bufferedImageMap;

    }



    private static BufferedImage getScaledInstance(BufferedImage img, int targetWidth, int targetHeight) {
        int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB
                : BufferedImage.TYPE_INT_ARGB;
        BufferedImage tmp = new BufferedImage(targetWidth, targetHeight, type);
        Graphics graphics = tmp.getGraphics();
        graphics.setColor(Color.white);
        graphics.fillRect(0, 0, targetWidth, targetHeight);

        BigDecimal imgWidth = new BigDecimal(img.getWidth());
        BigDecimal imgHeight = new BigDecimal(img.getHeight());
        BigDecimal targetImgWidth = new BigDecimal(targetWidth);
        BigDecimal targetImgHeight = new BigDecimal(targetHeight);

        BigDecimal imgProportion = imgWidth.divide(imgHeight,2, RoundingMode.FLOOR);
        BigDecimal targetProportion = targetImgWidth.divide(targetImgHeight,2, RoundingMode.FLOOR);

        Image resizedImg = null;
        if (imgProportion.compareTo(targetProportion)>=0) {
            BigDecimal resizedHeight = imgHeight.multiply(targetImgWidth).divide(imgWidth,0,RoundingMode.CEILING);

            resizedImg = img.getScaledInstance(targetWidth,resizedHeight.intValue(),Image.SCALE_AREA_AVERAGING);
            graphics.drawImage(resizedImg,0,(targetHeight-resizedHeight.intValue())/2,null);
            graphics.dispose();
        }
        else {
            BigDecimal resizedWidth = imgWidth.multiply(targetImgHeight).divide(imgHeight,0,RoundingMode.CEILING);

            resizedImg = img.getScaledInstance(resizedWidth.intValue(),targetHeight,Image.SCALE_AREA_AVERAGING);
            graphics.drawImage(resizedImg,(targetWidth-resizedWidth.intValue())/2,0,null);
            graphics.dispose();
        }

        return tmp;
    }

}
